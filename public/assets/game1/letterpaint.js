(function(){
  var container = document.querySelector('#container');

  /* botones */
  var startbutton = document.querySelector('#intro button');
  var exitbutton = document.querySelector('#salir');
  var winbutton = document.querySelector('#win button');
  var reloadbutton = document.querySelector('#reload');
  var errorbutton = document.querySelector('#error button');

  /* variables canvas */
  var c = document.querySelector('canvas');
  var cx = c.getContext('2d');
  var letter = null;
  var fontsize = 300;
  var paintcolour = [240, 240, 240];
  var textcolour = [255, 30, 20];
  var xoffset = 0;
  var yoffset = 0;
  var linewidth = 20;
  var pixels = 0;
  var letterpixels = 0;

  /* variables del mouse y touch */
  var mousedown = false;
  var touched = false;
  var oldx = 0;
  var oldy = 0;

  /* Variables de juego */
  var state = 'intro';
  var currentstate;
  var intentosbuenos = 0;
  var intentosmalos = 0;
  var nivel = 1;

  function init() {
    xoffset = container.offsetLeft;
    yoffset = container.offsetTop;
    fontsize = container.offsetHeight / 1.5;
    linewidth = container.offsetHeight / 19;
    paintletter();
    setstate('intro');
  }

  function showerror() {
    setstate('error');
    score(intentosmalos,intentosbuenos);
  }

  function setstate(newstate) {
    state = newstate;
    container.className = newstate;
    currentsate = state;
  }
  function moreneeded() {
    setstate('play');
    mousedown = false;
  }
  function retry(ev) {
    mousedown = false;
    oldx = 0;
    oldy = 0;
    paintletter(letter);
    intentosmalos = intentosmalos + 1;
    score(intentosmalos,intentosbuenos);
    /*niveles("Nivel",500);
    niveles(nivel,750);*/
  }
  function winner() {
    paintletter();
    score(intentosmalos,intentosbuenos);
    /*niveles("Nivel",500);
    niveles(nivel,750);*/
  }
  function start() {
    paintletter(letter);
    score(intentosmalos,intentosbuenos);
    /*niveles("Nivel",500);
    niveles(nivel,750);*/
  }
  function cancel() {
    paintletter();
    intentosbuenos = intentosbuenos + 1;
    score(intentosmalos,intentosbuenos);
    /*niveles("Nivel",500);
    niveles(nivel,750);*/
  }
  function eventosalir(){
    setstate('intro');
    intentosbuenos = 0;
    intentosmalos = 0;
    nivel = 1;
  }

  function score(num,num2){
    cx.fillStyle = "red";
    cx.font = "bold 90px helvetica";
    cx.fillText(num,100,100);
    cx.fillStyle = "green";
    cx.fillText(num2,200,100);
  }
  function niveles(level,num){
    cx.fillStyle = "#FF4500";
    cx.font = "bold 90px helvetica";
    cx.fillText(level,num,600);
  }

/* funciones para dibujar figuras */

  function dibujaTriangulo(){
    cx.strokeStyle = "#8B0000";
    cx.lineWidth = 40;
    cx.beginPath();
    cx.moveTo(670,190);
    cx.lineTo(500,390);
    cx.lineTo(840,390);
    cx.closePath();
    cx.stroke();
  }
  function dibujaCuadrado(){
    cx.strokeStyle = "#8B0000";
    cx.lineWidth = 40;
    cx.strokeRect(530,160,260,260);
  }
  function dibujaRectangulo(){
    cx.strokeStyle = "#8B0000";
    cx.lineWidth = 40;
    cx.strokeRect(480,160,370,220);
  }
  function dibujaTrapecio(){
    cx.strokeStyle = "#8B0000";
    cx.lineWidth = 40;
    cx.beginPath();
    cx.moveTo(510,190);
    cx.lineTo(770,190);
    cx.lineTo(840,390);
    cx.lineTo(440,390);
    cx.closePath();
    cx.stroke();
  }
  function dibujaHexagono(){
    cx.strokeStyle = "#8B0000";
    cx.lineWidth = 40;
    cx.beginPath();
    cx.moveTo(650, 180);
    cx.lineTo(800, 300);
    cx.lineTo(750, 450);
    cx.lineTo(550, 450);
    cx.lineTo(500, 300);
    cx.closePath();
    cx.stroke();
  }
  function dibujaOvalo(){
    cx.strokeStyle = "#8B0000";
    cx.lineWidth = 40;
    cx.save();
    cx.scale(1.2, 0.85);
    cx.translate(-110,-10);
    cx.beginPath();
    cx.arc(670, 320, 160, 0, Math.PI * 2);
    cx.stroke();
    cx.restore();
  }
  function dibujaCirculo(){
    cx.strokeStyle = "#8B0000";
    cx.lineWidth = 40;
    cx.beginPath();
    cx.arc(670, 320, 160, 0, Math.PI * 2);
    cx.stroke();
  }

  function paintletter(retryletter) {
    c.width = container.offsetWidth;
    c.height = container.offsetHeight;

    cx.shadowOffsetX = 2;
    cx.shadowOffsetY = 2;
    cx.shadowBlur = 4;
    cx.shadowColor = '#666';

    cx.textBaseline = 'baseline';
    cx.lineWidth = linewidth;
    cx.lineCap = 'round';

/*    if(intentosbuenos === 2){
      nivel = nivel + 1;
      intentosbuenos = 0;
      intentosmalos  = 0;
      cx.lineWidth = cx.linewidth-10;
      opc = opc + 4;
    }
*/
  var opc = Math.floor((Math.random() * 7) + 1);
  var opc2 = opc;
  
  if(opc === opc2){
    opc = Math.floor((Math.random() * 7) + 1);
  }
    switch(opc) {
    case 1:
        //cx.scale(0.5,0.5);
        dibujaTriangulo();
        break;
    case 2:
        dibujaCuadrado();
        break;
    case 3:
        dibujaRectangulo();
        break;
    case 4:
        dibujaCirculo();
        break;
    case 5:
        dibujaTrapecio();
        break;
    case 6:
        dibujaHexagono();
        break;
    case 7:
        dibujaOvalo();
        break;
    default:
    }

    cx.lineWidth = 30;
    cx.strokeStyle = "rgb(250,250,250)";

    pixels = cx.getImageData(0, 0, c.width, c.height);
    letterpixels = getpixelamount(
      textcolour[0],
      textcolour[1],
      textcolour[2]
    );
    cx.shadowOffsetX = 0;
    cx.shadowOffsetY = 0;
    cx.shadowBlur = 0;
    cx.shadowColor = '#333';
    setstate('play');
  }

  function getpixelamount(r, g, b) {
    var pixels = cx.getImageData(0, 0, c.width, c.height);
    var all = pixels.data.length;
    var amount = 0;
    for (i = 0; i < all; i += 4) {
      if (pixels.data[i] === r &&
          pixels.data[i+1] === g &&
          pixels.data[i+2] === b) {
        amount++;
      }
    }
    return amount;
  }

  function paint(x, y) {
    var rx = x - xoffset;
    var ry = y - yoffset;
    var colour = pixelcolour(x, y);
    if( colour.r === 0 && colour.g === 0 && colour.b === 0) {
      showerror();
    } else {
      cx.beginPath();
      if (oldx > 0 && oldy > 0) {
        cx.moveTo(oldx, oldy);
      }
      cx.lineTo(rx, ry);
      cx.stroke();
      cx.closePath();
      oldx = rx;
      oldy = ry;
    }
  }

  function pixelcolour(x, y) {
    var index = ((y * (pixels.width * 4)) + (x * 4));
    return {
      r:pixels.data[index],
      g:pixels.data[index + 1],
      b:pixels.data[index + 2],
      a:pixels.data[index + 3]
    };
  }

  function pixelthreshold() {
    if (state !== 'error') {
      if (getpixelamount(
        paintcolour[0],
        paintcolour[1],
        paintcolour[2]
      ) / letterpixels > 0.35) {
       setstate('win');
      }
    }
  }

  function onmouseup(ev) {
    ev.preventDefault();
    oldx = 0;
    oldy = 0;
    mousedown = false;
    pixelthreshold();
  }
  function onmousedown(ev) {
    ev.preventDefault();
    mousedown = true;
  }
  function onmousemove(ev) {
    ev.preventDefault();
    if (mousedown) {
      paint(ev.clientX, ev.clientY);
      ev.preventDefault();
    }
  }

  /* eventos touch */

  function ontouchstart(ev) {
    touched = true;
  }
  function ontouchend(ev) {
    touched = false;
    oldx = 0;
    oldy = 0;
    pixelthreshold();
  }
  function ontouchmove(ev) {
    if (touched) {
      paint(
        ev.changedTouches[0].pageX,
        ev.changedTouches[0].pageY
      );
      ev.preventDefault();
    }
  }

  /* eventos de botones */

  errorbutton.addEventListener('click', retry, false);
  reloadbutton.addEventListener('click', cancel, false);
  startbutton.addEventListener('click', start, false);
  exitbutton.addEventListener('click', eventosalir, false);

  /* funciones de eventos del mouse */

  c.addEventListener('mouseup', onmouseup, false);
  c.addEventListener('mousedown', onmousedown, false);
  c.addEventListener('mousemove', onmousemove, false);
  c.addEventListener('touchstart', ontouchstart, false);
  c.addEventListener('touchend', ontouchend, false);
  c.addEventListener('touchmove', ontouchmove, false);

  window.addEventListener('load',init, false);
  window.addEventListener('resize',init, false);

})();