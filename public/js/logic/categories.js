$(document).ready(function () {
    handleList_Category();
    showCreate_Category();
});

var showCreate_Category = function() {
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Category').val() }, 
        url:"/Admin/Categories/getCreate",
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_create').html(res);
        },
        error: function () {
            showNotify('<strong>Crear Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDetail_Category = function($elem) {
    console.log(event);
}

var showEdit_Category = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Category').val() }, 
        url:"/Admin/Categories/getEdit/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_edit').html(res);
            $('#mdlEditCategory').modal('show');
        },
        error: function () {
            showNotify('<strong>Editar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDelete_Category = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Category').val() }, 
        url:"/Admin/Categories/getDelete/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_delete').html(res);
            $('#mdlDeleteCategory').modal('show');
        },
        error: function () {
            showNotify('strong>Editar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var handleList_Category = function(page) {
    var ntfy = null;
    $.ajax({
        data: { 'page': page},
        headers: {'X-CSRF': $('#tknfrmList_Category').val() }, 
        url:"/Admin/Categories/getList",
        type:"get",
        beforeSend: function () {
            ntfy = $.notify({
                title: '<strong>Listar Categor&iacute;as: </strong><br/>',
                message: 'Preparando Consulta'
            },{
                type: 'info',
                newest_on_top: false,
                offset: { x : 20, y : 40 },
                placement: {
                    from: "bottom",
                    align: "right"
                }
            });
        },
        success: function (res) {
            $('#view_list').html(res);
            ntfy.update({
                message: 'Operaci&oacute;n Realizada',
                type: 'success'
            });
        },
        error: function (xhr, err, ex) {
            ntfy.update({
                message: 'Error del servidor. Intente m&aacute;s tarde <br><i>' + ex + '</i>',
                type: 'danger'
            });
        },
    });
}

var sRole = '';
var handleSubmit_CreateCategory = function(event) {
    event.preventDefault();
    if( isEmptyFields('#txtCreateDesc') ) {
        var bsalert = new BSAlert('Campo Vac&iacute;o', TypeAlert.Warning);
        $('#create-alert').html(bsalert.getHtml());
    }
    else {
        $.ajax({
            data: {
                _token: $('#tknCreate').val(),
                sCreateDesc: $('#txtCreateDesc').val()
            },
            headers: {'X-CSRF': $('#tknCreate').val() }, 
            url:"/Admin/Categories/store",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.status == 201) {
                    $('#mdlCreateCategory').modal('hide');
                    showNotify('<strong>Crear Categor&iacute;a</strong><br/>', res.message, res.label);
                    handleList_Category(-1);
                    showCreate_Category();
                } else {
                    var alert = new BSAlert(res.message, TypeAlert.Warning);
                    $('#create-alert').html(alert.getHtml());
                }
            },
            error: function (xhr, err, ex) {
                console.log(ex);
                showNotify('<strong>Crear Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleCancel_CreateCategory = function(event) {
    $('#mdlCreateCategory').modal('hide');
}

var handleSubmit_EditCategory = function(event) {
    event.preventDefault();
    if( isEmptyFields('#txtEditDesc') ) {
        var bsalert = new BSAlert('Campo Vac&iacute;o', TypeAlert.Warning);
        $('#edit-alert').html(bsalert.getHtml());
    } 
    else {
        $.ajax({
            data: {
                _token: $('#tknEdit').val(),
                sEditId: $('#txtEditId').val(),
                sEditDesc: $('#txtEditDesc').val()
            },
            headers: {'X-CSRF': $('#tknEdit').val() }, 
            url:"/Admin/Categories/save",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.status == 200) {
                    $('#mdlEditCategory').modal('hide');
                    showNotify('<strong>Editar Categor&iacute;a</strong><br/>', res.message, res.label);
                    handleList_Category(-1);
                } else {
                    var alert = new BSAlert(res.message, TypeAlert.Warning);
                    $('#edit-alert').html(alert.getHtml());
                }
            },
            error: function () {
                showNotify('<strong>Editar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleCancel_EditCategory = function(event) {
    $('#mdlEditCategory').modal('hide');
}

var handleSubmit_DeleteCategory = function(event) {
    event.preventDefault();
    $('#mdlDeleteCategory').modal('hide');
    
    $.ajax({
        data: {
            _token: $('#tknDelete').val(),
            sDestroyId: $('#txtDestroyId').val()
        },
        headers: {'X-CSRF': $('#tknDelete').val() }, 
        url:"/Admin/Categories/delete",
        type:"post",
        beforeSend: function () {

        },
        success: function (res) {
            if (res.status == 200) {
                showNotify('<strong>Eliminar Categor&iacute;a</strong><br/>', res.message, res.label);
                handleList_Category(-1);
            } else {
                var alert = new BSAlert(res.message, TypeAlert.Warning);
                $('#delete-alert').html(alert.getHtml());
            }
        },
        error: function (xhr, err, ex) {
            console.log(ex);
            showNotify('<strong>Eliminar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });    
}

var handleCancel_DeleteCategory = function(event) {
    event.preventDefault();
    $('#view_delete').html('<!--empty-->');
    $('#mdlDeleteCategory').modal('hide');
}

var isEmptyFields = function(selc) {
    sRole = $(selc).val().trim();

    if(sRole == '') {
        return true;
    } else {
        return false;
    }
}

var showNotify = function(_title, _message, _type) {
    var ntfy = $.notify({
        title: _title,
        message: _message 
    },{
        type: _type,
        newest_on_top: false,
        offset: { x : 20, y : 40 },
        placement: {
            from: "bottom",
		    align: "right"
        }
    });
}