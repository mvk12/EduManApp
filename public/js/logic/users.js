$(document).ready(function() {
    handleList_User(-1);
    showCreate_User();
});

var showCreate_User = function() {
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknUsers').val() }, 
        url:"/Admin/Users/getCreate",
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_create').html(res);
            $('#selecRole_CreateUser').change(function(event){
                $('#txtCreateRole').val($(this).val()); 
            });
        },
        error: function () {
            showNotify('Vista: Crear Usuario', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var s1 = null;
var s2 = null;

var showDetail_User = function($elem) {
    var id = $elem.data('id');

    $prgssbar = $('.progress-bar');

    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknUsers').val() }, 
        url:"/Admin/Users/getDetails/" + id,
        type:"get",
        beforeSend: function () {
            clearTimeout(s1);
            clearTimeout(s2);
            $('#view_details').html('');
            $prgssbar.addClass('progress-bar-info active').css('width', '50%');
        },
        success: function (res) {
            $('#view_details').html(res);
            $prgssbar.removeClass('progress-bar-info').addClass('progress-bar-success');
        },
        error: function (err) {
            console.log(err);
            showNotify('Mostrar Usuario', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            $prgssbar.removeClass('progress-bar-info').addClass('progress-bar-danger');
        },
        complete: function() {
            $prgssbar.removeClass('active').css('width', '100%');
            s1 = setTimeout(function(){
                $prgssbar.css('width', '0%');
            }, 1000);
            s2 = setTimeout(function(){
                $prgssbar.removeClass('progress-bar-success progress-bar-danger');
            }, 1500);
        }
    });
}

var showEdit_User = function($elem) {
    var id = $elem.data('id');

    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknUsers').val() }, 
        url:"/Admin/Users/getEdit/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_edit').html(res);
            $('#frmUserChangePwd').slideToggle();
            $('#mdlEditUser').modal('show');
        },
        error: function (err) {
            console.log(err);
            showNotify('<strong>Mostrar Usuario: </strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDelete_User = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {
            'X-CSRF': $('#tknUsers').val()
        },
        url: '/Admin/Users/getDelete/'+id,
        type: 'get',
        beforeSend: function() {

        },
        success: function(res) {
            $('#view_delete').html(res);
            $('#mdlDeleteUser').modal('show');
        },
        error: function(err) {
            console.log(err);
            showNotify('Eliminar Usuario: Error', 'El sistema no pudo completar su solicitud.<br>Intente m&aacute;s tarde.', 'danger');
        }

    })
    
}

var handleList_User = function(page) {
    var ntfy = null;
    $.ajax({
        data: { 'page': page},
        headers: {'X-CSRF': $('#tknUsers').val() }, 
        url:"/Admin/Users/getList",
        type:"get",
        beforeSend: function () {
            ntfy = $.notify({
                title: '<strong>Listar Usuarios: </strong><br/>',
                message: 'Preparando Cosulta' 
            },{
                type: 'info',
                newest_on_top: false,
                offset: { x : 20, y : 40 },
                placement: {
                    from: "bottom",
                    align: "right"
                }
            });
        },
        success: function (res) {
            $('#view_list').html(res);
            ntfy.update({
                message: 'Operaci&oacute;n Realizada',
                type: 'success'
            });
        },
        error: function (xhr, err, ex) {
            console.log(ex);
            ntfy.update({
                message: 'Error del servidor. Intente m&aacute;s tarde <br><i>' + ex + '</i>',
                type: 'danger'
            });
        },
    });
}

var handleSubmit_CreateUser = function(event) {
    event.preventDefault();

    if(validateFields_Create()) {
        $.ajax({
            data: dataCreate,
            headers: {'X-CSRF': $('#tknCreate').val() }, 
            url:"/Admin/Users/store",
            type:"post",
            dataType: "json",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.status == 200 || res.status == 201) {
                    $('#mdlCreateUser').modal('hide');
                    showNotify('<strong>Crear Categor&iacute;a</strong><br>', 'Operación Realizada: <br> ' + res.message, res.label);
                    handleList_User(-1);
                    showCreate_User();
                } else if(res.status == 400 || res.status == 404) {
                    var alt = new BSAlert(res.message, TypeAlert.Warning);
                    $('#alert-section').html(alt.getHtml());
                }
            },
            error: function (err) {
                console.log(err);
                showNotify('<strong>Crear Categor&iacute;a</strong><br>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    } else {
        var alt = new BSAlert("Existen Campos Vac&iacute;os", TypeAlert.Warning);
        $('#alert-section').html(alt.getHtml());
    }
}

var handleCancel_CreateUser = function(event) {
    console.log(event);
    $('#mdlCreateUser').modal('hide');
}

var handleSubmit_EditUser = function(event) {
    event.preventDefault();
    if(validateFields_Edit()) {
        $.ajax({
            type: "post",
            url: "/Admin/Users/save",
            data: dataEdit,
            dataType: "json",
            beforeSend: function() {

            },
            success: function (res) {
                if(res.status == 200) {
                    showNotify('<strong>Actualizar Usuario: </strong><br/>', res.message, 'success');
                    $('#mdlEditUser').modal('hide');
                    handleList_User(-1);
                } else if (res.status == 400 || res.status == 404) {
                    var al = new BSAlert(res.message, TypeAlert.Warning);
                    $('#alert-section-edit').html(al.getHtml());
                } else {
                    var al = new BSAlert(res.message, TypeAlert.Danger);
                    $('#alert-section-edit').html(al.getHtml());
                }
            },
            error: function(xhr, err, ex) {
                var al = new BSAlert('<strong>Actualizar Usuario: </strong>', TypeAlert.Warning);
                $('#alert-section-edit').html(al.getHtml());
            },
            complete: function() {

            },
        });
    } else {
        var al = new BSAlert('Campos Vac&iacute;os', TypeAlert.Warning);
        $('#alert-section-edit').html(al.getHtml());
    }
}

var handleCancel_EditUser = function(event) {
    $('#view_edit').html('');
    $('#mdlEditUser').modal('hide');
}

var bChgPwd = false;

var handleAllowChangePassword = function(event) {
    bChgPwd = !bChgPwd; 
    //$('#frmUserChangePwd').slideToggle();
    if(bChgPwd){
        $('#frmUserChangePwd').slideDown();
        $('#txtEditPwd1, #txtEditPwd2').removeAttr('disabled');
    }else{
        $('#frmUserChangePwd').slideUp();
        $('#txtEditPwd1, #txtEditPwd2').attr('disabled', 'disabled');
    }
}

var handleSubmit_DeleteUser = function(event) {
    event.preventDefault();
    var data = {
        _token : $('#tknDelete').val(),
        sDeleteId: $('#txtDeleteId').val()
    }

    if (data.sDeleteId == '') {

    } else {
        $.ajax({
            data: data,
            headers: {'X-CSRF': $('#tknDelete').val() },
            url: '/Admin/Users/delete',
            type: 'post',
            dataType: "json",
            beforeSend: function() {

            },
            success: function (response) {
                $('#mdlDeleteUser').modal('hide');
                showNotify('<strong> Eliminar Usuario:</strong><br/>', 'Operaci&oacute;n Realizada', 'success');
                handleList_User(-1);
            },
            error: function() {
                console.log(err);
                showNotify('<strong> Eliminar Usuario:</strong><br/>', 'El sistema no pudo completar su solicitud.<br>Intente m&aacute;s tarde.', 'danger');
            },
        });
    }
}

var handleCancel_DeleteUser = function(event) {
    $('#view_delete').html('');
    $('#mdlDeleteUser').modal('hide');
}

var handleSubmit_ChngPwd = function(event) {
    event.preventDefault();
}

var handleCancel_ChngPwd = function(event) {
    handleAllowChangePassword();
    $('#mdlEditUser').modal('hide');
}

var dataCreate = {
    _token: '',
    sCreateName: '',
    sCreateNick: '',
    sSelecRoleCreateUser: '',
    sCreatePwd1: '',
    sCreatePwd2: '',
    sCreateMail: '',
    sCreateImagePath: ''
}

var validateFields_Create = function() {

    dataCreate['_token'] = $('#tknCreate').val().trim(); 
    if(dataCreate['_token'] == '')
        return false;
    dataCreate['sCreateName'] = $('#txtCreateName').val().trim(); 
    if(dataCreate['sCreateName'] == '')
        return false;
    dataCreate['sCreateNick'] = $('#txtCreateNick').val().trim();
    if(dataCreate['sCreateNick'] == '')
        return false;
    dataCreate['sSelecRoleCreateUser'] = $('#selecRole_CreateUser').val().trim();
    if(dataCreate['sSelecRoleCreateUser'] == '')
        return false;
    dataCreate['sCreatePwd1'] = $('#txtCreatePwd1').val().trim();
    if(dataCreate['sCreatePwd1'] == '')
        return false;
    dataCreate['sCreatePwd2'] = $('#txtCreatePwd2').val().trim();
    if(dataCreate['sCreatePwd2'] == '')
        return false;
    dataCreate['sCreateMail'] = $('#txtCreateMail').val().trim();
    if(dataCreate['sCreateMail'] == '')
        return false;
    dataCreate['sCreateImagePath'] = $('#txtCreateImagePath').val().trim()
    
    return true;
}

var dataEdit = {
    _token: '',
    sEditId: '',
    sEditName: '',
    sEditNick: '',
    sSelecRoleEditUser: '',
    sEditMail: '',
    sEditImagePath: ''
}

var validateFields_Edit = function() {
    dataEdit['_token'] = $('#tknEdit').val().trim(); 
    if(dataEdit['_token'] == '')
        return false;
    dataEdit['sEditId'] = $('#txtEditId').val().trim();
    if(dataEdit['sEditId'] == '')
        return false;
    dataEdit['sEditName'] = $('#txtEditName').val().trim(); 
    if(dataEdit['sEditName'] == '')
        return false;
    dataEdit['sEditNick'] = $('#txtEditNick').val().trim();
    if(dataEdit['sEditNick'] == '')
        return false;
    dataEdit['sSelecRoleEditUser'] = $('#selecRole_EditUser').val().trim();
    if(dataEdit['sSelecRoleEditUser'] == '')
        return false;    
    dataEdit['sEditMail'] = $('#txtEditMail').val().trim();
    if(dataEdit['sEditMail'] == '')
        return false;
    dataEdit['sEditImagePath'] = $('#txtEditImagePath').val().trim()
    
    return true;
}

var OnChangeHandle = function($elem) {
    $elem.change(function(event){
        console.log(event);
        $('#txtCreateRole').val($(this).val());
    });
}

var showNotify = function(_title, _message, _type) {
    var ntfy = $.notify({
        title: _title,
        message: _message 
    },{
        type: _type,
        newest_on_top: false,
        offset: { x : 20, y : 40 },
        placement: {
            from: "bottom",
		    align: "right"
        }
    });
}