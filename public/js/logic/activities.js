$(document).ready(function () {
    handleList_Activity();
    showCreate_Activity();
});

var showCreate_Activity = function() {
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Activity').val() }, 
        url:"/Admin/Activities/getCreate",
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_create').html(res);
        },
        error: function () {
            showNotify('<strong>Crear Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var s1 = null;
var s2 = null;
var showDetail_Activity = function($elem) {
    var id = $elem.data('id');

    $prgssbar = $('.progress-bar');

    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknActivities').val() }, 
        url:"/Admin/Activities/getDetails/" + id,
        type:"get",
        beforeSend: function () {
            clearTimeout(s1);
            clearTimeout(s2);
            $('#view_details').html('');
            $prgssbar.addClass('progress-bar-info active').css('width', '50%');
        },
        success: function (res) {
            $('#view_details').html(res);
            $prgssbar.removeClass('progress-bar-info').addClass('progress-bar-success');
        },
        error: function (xhr, err, ex) {
            console.log(ex);
            showNotify('Mostrar Usuario', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            $prgssbar.removeClass('progress-bar-info').addClass('progress-bar-danger');
        },
        complete: function() {
            $prgssbar.removeClass('active').css('width', '100%');
            s1 = setTimeout(function(){
                $prgssbar.css('width', '0%');
            }, 1000);
            s2 = setTimeout(function(){
                $prgssbar.removeClass('progress-bar-success progress-bar-danger');
            }, 1500);
        }
    });
}

var showEdit_Activity = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Activity').val() }, 
        url:"/Admin/Activities/getEdit/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_edit').html(res);
            $('#mdlEditActivity').modal('show');
        },
        error: function () {
            showNotify('<strong>Editar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDelete_Activity = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Activity').val() }, 
        url:"/Admin/Activities/getDelete/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_delete').html(res);
            $('#mdlDeleteActivity').modal('show');
        },
        error: function () {
            showNotify('strong>Editar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var handleList_Activity = function(page) {
    var ntfy = null;
    $.ajax({
        data: { 'page': page},
        headers: {'X-CSRF': $('#tknfrmList_Activity').val() }, 
        url:"/Admin/Activities/getList",
        type:"get",
        beforeSend: function () {
            ntfy = $.notify({
                title: '<strong>Listar Actividades: </strong><br/>',
                message: 'Preparando Consulta'
            },{
                type: 'info',
                newest_on_top: false,
                offset: { x : 20, y : 40 },
                placement: {
                    from: "bottom",
                    align: "right"
                }
            });
        },
        success: function (res) {
            $('#view_list').html(res);
            ntfy.update({
                message: 'Operaci&oacute;n Realizada',
                type: 'success'
            });
        },
        error: function (xhr, err, ex) {
            ntfy.update({
                message: 'Error del servidor. Intente m&aacute;s tarde <br><i>' + ex + '</i>',
                type: 'danger'
            });
        },
    });
}

var handleSubmit_CreateActivity = function(event) {
    event.preventDefault();
    if (!validateFields_Create()) {
        console.log(dataCreate);
        var bsalert = new BSAlert('Campo Vac&iacute;o', TypeAlert.Warning);
        $('#create-alert').html(bsalert.getHtml());
    } else {
        $.ajax({
            data: dataCreate,
            headers: {'X-CSRF': $('#tknCreate').val() }, 
            url:"/Admin/Activities/store",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.status == 201) {
                    $('#mdlCreateActivity').modal('hide');
                    showNotify('<strong>Crear Actividades:</strong><br/>', res.message, res.label);
                    handleList_Activity(-1);
                    //showCreate_Activity();
                } else {
                    var alert = new BSAlert(res.message, TypeAlert.Warning);
                    $('#create-alert').html(alert.getHtml());
                }
            },
            error: function (xhr, err, ex) {
                console.log(ex);
                showNotify('<strong>Crear Actividades:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleCancel_CreateActivity = function(event) {
    $('#mdlCreateActivity').modal('hide');
}

var handleSubmit_EditActivity = function(event) {
    event.preventDefault();
    if( !validateFields_Edit() ) {
        var bsalert = new BSAlert('Existen Campos Vac&iacute;os', TypeAlert.Warning);
        $('#edit-alert').html(bsalert.getHtml());
    } else {
        $.ajax({
            data: dataEdit,
            headers: {'X-CSRF': $('#tknCreate').val() }, 
            url:"/Admin/Activities/save",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                if (res.status == 200) {
                    $('#mdlEditActivity').modal('hide');
                    showNotify('<strong>Editar Categor&iacute;a</strong><br/>', res.message, res.label);
                    handleList_Activity(-1);
                } else {
                    var alert = new BSAlert(res.message, TypeAlert.Warning);
                    $('#edit-alert').html(alert.getHtml());
                }
            },
            error: function () {
                showNotify('<strong>Editar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleCancel_EditActivity = function(event) {
    $('#mdlEditActivity').modal('hide');
}

var handleSubmit_DeleteActivity = function(event) {
    event.preventDefault();
    $('#mdlDeleteActivity').modal('hide');
    
    $.ajax({
        data: {
            _token: $('#tknDelete').val(),
            sDeleteId: $('#txtDeleteId').val().trim()
        },
        headers: {'X-CSRF': $('#tknDelete').val() }, 
        url:"/Admin/Activities/delete",
        type:"post",
        beforeSend: function () {

        },
        success: function (res) {
            if (res.status == 200) {
                showNotify('<strong>Eliminar Categor&iacute;a</strong><br/>', res.message, res.label);
                handleList_Activity(-1);
            } else {
                var alert = new BSAlert(res.message, TypeAlert.Warning);
                $('#delete-alert').html(alert.getHtml());
            }
        },
        error: function (xhr, err, ex) {
            console.log(ex);
            showNotify('<strong>Eliminar Categor&iacute;a</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });    
}

var handleCancel_DeleteActivity = function(event) {
    event.preventDefault();
    $('#view_delete').html('<!--empty-->');
    $('#mdlDeleteActivity').modal('hide');
}

var dataCreate = {
    _token: '',
    sCreateName: '',
    sCreateDesc: '',
    iTypeActivity_CreateActivity: '',
    iCategory_CreateActivity: ''
}

var validateFields_Create = function() {
    dataCreate['_token'] = $('#tknCreate').val().trim(); 
    if(dataCreate['_token'] == '')
        return false;
    dataCreate['sCreateName'] = $('#txtCreateName').val().trim();
    if(dataCreate['sCreateName'] == '')
        return false;
    dataCreate['sCreateDesc'] = $('#txtCreateDesc').val().trim(); 
    if(dataCreate['sCreateDesc'] == '')
        return false;
    dataCreate['iTypeActivity_CreateActivity'] = $('#selectCreate_TypeActivity').val().trim();
    if(dataCreate['iTypeActivity_CreateActivity'] == '')
        return false;
    dataCreate['iCategory_CreateActivity'] = $('#selectCreate_Category').val().trim();
    if(dataCreate['iCategory_CreateActivity'] == '')
        return false;
    return true;
}

var dataEdit = {
    _token: '',
    sEditId: '',
    sCreateName: '',
    sCreateDesc: '',
    iTypeActivity_CreateActivity: '',
    iCategory_CreateActivity: ''
}

var validateFields_Edit = function() {
    dataEdit['_token'] = $('#tknCreate').val().trim(); 
    if(dataEdit['_token'] == '')
        return false;
        dataEdit['sEditId'] = $('#tknEditId').val().trim(); 
    if(dataEdit['sEditId'] == '')
        return false;
    dataEdit['sEditName'] = $('#txtEditName').val().trim();
    if(dataEdit['sEditName'] == '')
        return false;
    dataEdit['sEditDesc'] = $('#txtEditDesc').val().trim(); 
    if(dataEdit['sEditDesc'] == '')
        return false;
    dataEdit['iTypeActivity_EditActivity'] = $('#selectTypeActivity_EditActivity').val().trim();
    if(dataEdit['iTypeActivity_EditActivity'] == '')
        return false;
    dataEdit['iCategory_EditActivity'] = $('#selectCategory_EditActivity').val().trim();
    if(dataEdit['iCategory_EditActivity'] == '')
        return false;
    return true;
}
var showNotify = function(_title, _message, _type) {
    var ntfy = $.notify({
        title: _title,
        message: _message 
    },{
        type: _type,
        newest_on_top: false,
        offset: { x : 20, y : 40 },
        placement: {
            from: "bottom",
		    align: "right"
        }
    });
}