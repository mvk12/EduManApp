$(document).ready(function () {
    handleList_Role();
    showCreate_Role();
});

var showCreate_Role = function() {
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Role').val() }, 
        url:"/Admin/Roles/getCreate",
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_create').html(res);
        },
        error: function () {
            showNotify('<strong>Crear Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDetail_Role = function($elem) {
    console.log(event);
}

var showEdit_Role = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Role').val() }, 
        url:"/Admin/Roles/getEdit/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_edit').html(res);
            $('#mdlEditRole').modal('show');
        },
        error: function () {
            showNotify('<strong>Editar Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDelete_Role = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_Role').val() }, 
        url:"/Admin/Roles/getDelete/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_delete').html(res);
            $('#mdlDeleteRole').modal('show');
        },
        error: function () {
            showNotify('Editar Rol', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var handleList_Role = function(page) {
    var ntfy = null;
    $.ajax({
        data: { 'page': page},
        headers: {'X-CSRF': $('#tknfrmList_Role').val() }, 
        url:"/Admin/Roles/getList",
        type:"get",
        beforeSend: function () {
            ntfy = $.notify({
                title: '<strong>Listar Usuarios: </strong><br/>',
                message: 'Preparando Cosulta' 
            },{
                type: 'info',
                newest_on_top: false,
                offset: { x : 20, y : 40 },
                placement: {
                    from: "bottom",
                    align: "right"
                }
            });
        },
        success: function (res) {
            $('#view_list').html(res);
            ntfy.update({
                message: 'Operaci&oacute;n Realizada',
                type: 'success'
            });
        },
        error: function (xhr, err, ex) {
            console.log(ex);
            ntfy.update({
                message: 'Error del servidor. Intente m&aacute;s tarde <br><i>' + ex + '</i>',
                type: 'danger'
            }); 
        },
    });
}

var sRole = '';
var handleSubmit_CreateRole = function(event) {
    event.preventDefault();
    if( isEmptyFields('#txtCreateDesc') ) {
        var bsalert = new BSAlert('Campo Vac&iacute;o', TypeAlert.Warning);
        $('#create-alert').html(bsalert.getHtml());
    }
    else {
        $.ajax({
            data: {
                _token: $('#tknCreate').val(),
                sCreateDesc: $('#txtCreateDesc').val()
            },
            headers: {'X-CSRF': $('#tknCreate').val() }, 
            url:"/Admin/Roles/store",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                if(res.status == 201) {
                    $('#mdlCreateRole').modal('hide');
                    showNotify('<strong>Crear Rol:</strong><br/>', res.message, 'success');
                    handleList_Role(-1);
                    showCreate_Role();
                } else {
                    var alert = new BSAlert(res.message, TypeAlert.Warning);
                    $('#create-alert').html(alert.getHtml());
                }
            },
            error: function (xhr, err, ex) {
                console.log(ex);
                showNotify('<strong>Crear Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleSubmit_EditRole = function(event) {
    event.preventDefault();
    if( isEmptyFields('#txtEditDesc') ) {
        var bsalert = new BSAlert('Campo Vac&iacute;o', TypeAlert.Warning);
        $('#edit-alert').html(bsalert.getHtml());
    } 
    else {
        $.ajax({
            data: {
                _token: $('#tknEdit').val(),
                sEditId: $('#txtEditId').val(),
                sEditDesc: $('#txtEditDesc').val()
            },
            headers: {'X-CSRF': $('#tknEdit').val() }, 
            url:"/Admin/Roles/save",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                $('#mdlEditRole').modal('hide');
                showNotify('<strong>Editar Rol:</strong><br/>', 'Operación Realizada: <br> ' + res, 'success');
                handleList_Role(-1);
            },
            error: function () {
                showNotify('<strong>Editar Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleIgnore_EditRole = function(event) {
    $('#txtEditId').val('');
    $('#txtEditDesc').val('');
    $('#mdlEditRole').modal('hide');
}

var handleSubmit_DeleteRole = function(event) {
    event.preventDefault();
    $('#mdlDeleteRole').modal('hide');
    
    $.ajax({
        data: {
            _token: $('#tknDelete').val(),
            sDestroyId: $('#txtDestroyId').val()
        },
        headers: {'X-CSRF': $('#tknDelete').val() }, 
        url:"/Admin/Roles/delete",
        type:"post",
        beforeSend: function () {

        },
        success: function (res) {
            if (res.status == 200) {
                showNotify('<strong>Eliminar Rol:</strong><br/>', res.message, res.label);
                handleList_Role(-1);
            } else {
                showNotify('<strong>Eliminar Rol:</strong><br/>', res.message, res.label);
            }
        },
        error: function (xhr, err, ex) {
            showNotify('<strong>Eliminar Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });    
}

var handleCancel_DeleteRole = function(event) {
    event.preventDefault();

    $('#mdlDeleteRole').modal('hide');
}

var isEmptyFields = function(selc) {
    sRole = $(selc).val().trim();

    if(sRole == '') {
        return true;
    } else {
        return false;
    }
}

var showNotify = function(_title, _message, _type) {
    var ntfy = $.notify({
        title: _title,
        message: _message 
    },{
        type: _type,
        newest_on_top: false,
        offset: { x : 20, y : 40 },
        placement: {
            from: "bottom",
		    align: "right"
        }
    });
}