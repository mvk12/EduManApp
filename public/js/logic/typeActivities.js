$(document).ready(function () {
    handleList_TypeActivity();
    showCreate_TypeActivity();
});

var showCreate_TypeActivity = function() {
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_TypeActivity').val() }, 
        url:"/Admin/TypeActivities/getCreate",
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_create').html(res);
        },
        error: function () {
            showNotify('<strong>Crear Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDetail_TypeActivity = function($elem) {
    console.log(event);
}

var showEdit_TypeActivity = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_TypeActivity').val() }, 
        url:"/Admin/TypeActivities/getEdit/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_edit').html(res);
            $('#mdlEditTypeActivity').modal('show');
        },
        error: function () {
            showNotify('<strong>Editar Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var showDelete_TypeActivity = function($elem) {
    var id = $elem.data('id');
    $.ajax({
        data: {},
        headers: {'X-CSRF': $('#tknfrmList_TypeActivity').val() }, 
        url:"/Admin/TypeActivities/getDelete/" + id,
        type:"get",
        beforeSend: function () {

        },
        success: function (res) {
            $('#view_delete').html(res);
            $('#mdlDeleteTypeActivity').modal('show');
        },
        error: function () {
            showNotify('Editar Rol', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });
}

var handleList_TypeActivity = function(page) {
    var ntfy = null;
    $.ajax({
        data: { 'page': page},
        headers: {'X-CSRF': $('#tknfrmList_TypeActivity').val() }, 
        url:"/Admin/TypeActivities/getList",
        type:"get",
        beforeSend: function () {
            ntfy = $.notify({
                title: '<strong>Listar Usuarios: </strong><br/>',
                message: 'Preparando Cosulta' 
            },{
                type: 'info',
                newest_on_top: false,
                offset: { x : 20, y : 40 },
                placement: {
                    from: "bottom",
                    align: "right"
                }
            });
        },
        success: function (res) {
            $('#view_list').html(res);
            ntfy.update({
                message: 'Operaci&oacute;n Realizada',
                type: 'success'
            });
        },
        error: function (xhr, err, ex) {
            console.log(ex);
            ntfy.update({
                message: 'Error del servidor. Intente m&aacute;s tarde <br><i>' + ex + '</i>',
                type: 'danger'
            }); 
        },
    });
}

var sTypeActivity = '';
var handleSubmit_CreateTypeActivity = function(event) {
    event.preventDefault();
    if( isEmptyFields('#txtCreateDesc') ) {
        var bsalert = new BSAlert('Campo Vac&iacute;o', TypeAlert.Warning);
        $('#create-alert').html(bsalert.getHtml());
    }
    else {
        $.ajax({
            data: {
                _token: $('#tknCreate').val(),
                sCreateDesc: $('#txtCreateDesc').val()
            },
            headers: {'X-CSRF': $('#tknCreate').val() }, 
            url:"/Admin/TypeActivities/store",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                if(res.status == 201) {
                    $('#mdlCreateTypeActivity').modal('hide');
                    showNotify('<strong>Crear Rol:</strong><br/>', res.message, 'success');
                    handleList_TypeActivity(-1);
                    showCreate_TypeActivity();
                } else {
                    var alert = new BSAlert(res.message, TypeAlert.Warning);
                    $('#create-alert').html(alert.getHtml());
                }
            },
            error: function (xhr, err, ex) {
                console.log(ex);
                showNotify('<strong>Crear Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleSubmit_EditTypeActivity = function(event) {
    event.preventDefault();
    if( isEmptyFields('#txtEditDesc') ) {
        var bsalert = new BSAlert('Campo Vac&iacute;o', TypeAlert.Warning);
        $('#edit-alert').html(bsalert.getHtml());
    } 
    else {
        $.ajax({
            data: {
                _token: $('#tknEdit').val(),
                sEditId: $('#txtEditId').val(),
                sEditDesc: $('#txtEditDesc').val()
            },
            headers: {'X-CSRF': $('#tknEdit').val() }, 
            url:"/Admin/TypeActivities/save",
            type:"post",
            beforeSend: function () {

            },
            success: function (res) {
                $('#mdlEditTypeActivity').modal('hide');
                showNotify('<strong>Editar Rol:</strong><br/>', 'Operación Realizada: <br> ' + res, 'success');
                handleList_TypeActivity(-1);
            },
            error: function () {
                showNotify('<strong>Editar Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
            },
        });
    }
}

var handleIgnore_EditTypeActivity = function(event) {
    $('#txtEditId').val('');
    $('#txtEditDesc').val('');
    $('#mdlEditTypeActivity').modal('hide');
}

var handleSubmit_DeleteTypeActivity = function(event) {
    event.preventDefault();
    $('#mdlDeleteTypeActivity').modal('hide');
    
    $.ajax({
        data: {
            _token: $('#tknDelete').val(),
            sDestroyId: $('#txtDestroyId').val()
        },
        headers: {'X-CSRF': $('#tknDelete').val() }, 
        url:"/Admin/TypeActivities/delete",
        type:"post",
        beforeSend: function () {

        },
        success: function (res) {
            if (res.status == 200) {
                showNotify('<strong>Eliminar Rol:</strong><br/>', res.message, res.label);
                handleList_TypeActivity(-1);
            } else {
                showNotify('<strong>Eliminar Rol:</strong><br/>', res.message, res.label);
            }
        },
        error: function (xhr, err, ex) {
            showNotify('<strong>Eliminar Rol:</strong><br/>', 'Error del servidor <br> Intente m&aacute;s tarde ', 'danger');
        },
    });    
}

var handleCancel_DeleteTypeActivity = function(event) {
    event.preventDefault();

    $('#mdlDeleteTypeActivity').modal('hide');
}

var isEmptyFields = function(selc) {
    sTypeActivity = $(selc).val().trim();

    if(sTypeActivity == '') {
        return true;
    } else {
        return false;
    }
}

var showNotify = function(_title, _message, _type) {
    var ntfy = $.notify({
        title: _title,
        message: _message 
    },{
        type: _type,
        newest_on_top: false,
        offset: { x : 20, y : 40 },
        placement: {
            from: "bottom",
		    align: "right"
        }
    });
}