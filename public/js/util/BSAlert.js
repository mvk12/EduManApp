/**
 * @file		BSAlert.js
 * @author		Miguel Vazquez
 * @description	Script Constructor for Bootstrap´s Alert on frontend
 */

/** 
 * TypeAlert Enum for Alert Bootstrap
 */
var TypeAlert = {
	Success: "alert-success",
	Info: "alert-info",
	Warning: "alert-warning",
	Danger: "alert-danger",
	None: ""
};

/**
 * IsDismiss String for Dismissible Alert
 * */
var IsDismiss = 'alert-dismissible';

/**
 * Button Dismissible
 * */
var button = "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" +
	"<span aria-hidden=\"true\">&times;</span>" + "</button>";

/**
 * Constructor Default
 * */
var BSAlert = function() {
	this.Message = "";
	this.Type = TypeAlert.None;
	this.Dismiss = false;
};

/**
 * Constructor with params
 * @param _message String Message
 * @param _typeAlert TypeAlert 
 * @param _dismissible boolean Dismissible flag
 * */
var BSAlert = function(_message, _typeAlert, _dismissible) {
	this.Message = _message;
	this.Type = _typeAlert;
	if(_dismissible == undefined)
		this.Dismiss = true;
	else
		this.Dismiss = _dismissible;
};

/**
 * @method getHtml
 * @return String HTML of a BSAlert object
 * */
BSAlert.prototype.getHtml = function() {
	
	if(this.Message == "")
		return "";	
	
	var btn = "";
	if(this.Dismiss)
		btn = button;
	
	return '<div class ="alert ' + this.Type + ' ' + (this.Dismiss ? IsDismiss: '') +'">' + btn + this.Message +'</div>';
};