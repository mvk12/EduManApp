<?php
/*
|--------------------------------------------------------------------------
| FIX: 
| Maximum function nesting level of '100' reached, aborting!
|--------------------------------------------------------------------------
| Issue is caused by default xdebug.max_nesting_level which is 100.
|--------------------------------------------------------------------------
| The workaround for now is to increase xdebug.max_nesting_level 
| to a certain level say 200 or 300 or 400.
| I fixed mine by increasing xdebug.max_nesting_level to 120, 
| by adding the line below to bootstrap/autoload.php in the Laravel 5.1
|
*/
ini_set('xdebug.max_nesting_level', 120);

/*
|--------------------------------------------------------------------------
| Laravel Start Time
|--------------------------------------------------------------------------
|
*/
define('LARAVEL_START', microtime(true));

/*
|--------------------------------------------------------------------------
| Register The Composer Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader
| for our application. We just need to utilize it! We'll require it
| into the script here so that we do not have to worry about the
| loading of any our classes "manually". Feels great to relax.
|
*/

require __DIR__.'/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Include The Compiled Class File
|--------------------------------------------------------------------------
|
| To dramatically increase your application's performance, you may use a
| compiled class file which contains all of the classes commonly used
| by a request. The Artisan "optimize" is used to create this file.
|
*/

$compiledPath = __DIR__.'/cache/compiled.php';

if (file_exists($compiledPath)) {
    require $compiledPath;
}
