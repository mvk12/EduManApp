<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Roles */
        \EduManApp\Role::create([
            'desc' => 'root'
        ]);
        \EduManApp\Role::create([
            'desc' => 'tutor'
        ]);
        \EduManApp\Role::create([
            'desc' => 'child'
        ]);

        /* Users */
        \EduManApp\Usuario::create([
            'id' => 1,
            'username' => 'usr',
            'password' => '1234',
            'mail' => 'nomail@mail.com',
            'role' => 1
        ]);

        /* Perfils */
        \EduManApp\Perfil::create([
            'id' => 1,
            'fullname' => 'Miguel V&aacute;zquez',
            'pathimage' => ''
        ]);
    }
}
