<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensajes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('fromuser')->unsigned();
            $table->integer('touser')->unsigned();
            $table->string('header');
            $table->text('content');
            $table->boolean('status');
            $table->foreign('fromuser')->references('id')->on('usuarios');
            $table->foreign('touser')->references('id')->on('usuarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mensajes');
    }
}
