<?php

namespace EduManApp;

use Illuminate\Database\Eloquent\Model;

class Perfil extends Model
{
    protected $table = "perfils";

    protected $fillable = ['id', 'fullname', 'pathimage'];
}
