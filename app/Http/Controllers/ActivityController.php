<?php

namespace EduManApp\Http\Controllers;

use Illuminate\Http\Request;

use EduManApp\Http\Requests;

class ActivityController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }

    public function index(Request $request){
        return view('admin.activities.index');
    }
    
    public function store(Request $request){
        $code = 201;
        $message = 'Creado Satisfactoriamente';
        $label = 'success';

        $activity = \EduManApp\Activity::create([
            'name' => $request->sCreateName,
            'desc' => $request->sCreateDesc,
            'type' => $request->iTypeActivity_CreateActivity,
            'category' => $request->iCategory_CreateActivity
        ]);
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }
    
    public function save(Request $request){
        $code = 200;
        $message = 'Actualizado Satisfactoriamente';
        $label = 'success';

        $activity = \EduManApp\Activity::find($request->sEditId);

        if ($activity == null) {
            $code = 404;
            $message = 'Elemento Inexistente';
            $label = 'danger';
        } else {
            $activity->name = $request->sEditName;
            $activity->desc = $request->sEditDesc;
            $activity->type = $request->iTypeActivity_EditActivity;
            $activity->category = $request->iCategory_EditActivity;

            $activity->save();
        }
        
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }
    
    public function delete(Request $request){
        $code = 200;
        $message = 'Eliminado Satisfactoriamente';
        $label = 'success';

        $activity = \EduManApp\Activity::find($request->sDeleteId);
        if ($activity == null) {
            $code = 404;
            $message = 'Elemento Inexistente';
            $label = 'danger';
        } else {
            $activity->delete();
        }
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }
    
    public function getCreate(Request $request){
        $typeactivities = \EduManApp\TypeActivity::all();
        $categories = \EduManApp\Categoria::all();
        return view('admin.activities.create',['typeactivities' => $typeactivities, 'categories' => $categories]);
    }
    
    public function getEdit($id, Request $request){
        $activity =  \EduManApp\Activity::find($id);
        $typeactivities = \EduManApp\TypeActivity::all();
        $categories = \EduManApp\Categoria::all();

        return view('admin.activities.edit',['typeactivities' => $typeactivities, 'categories' => $categories, 'activity' => $activity]);
    }

    public function getDetails($id, Request $request){
        $activity = \EduManApp\Activity::find($id);
        $json = json_encode($activity);
        return view('admin.activities.details',['activity'=>$activity,'json'=> $json]);
    }
    
    public function getDelete($id, Request $request){
        $activity = \EduManApp\Activity::find($id);
        return view('admin.activities.delete', ['activity' => $activity]);
    }
    
    public function getList(Request $request){
        $activities = \EduManApp\Activity::all();
        return view('admin.activities.list',['activities'=>$activities]);
    }
}
