<?php

namespace EduManApp\Http\Controllers;

use Illuminate\Http\Request;

use EduManApp\Http\Requests;

class CategoryController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }
    public function index(Request $request){
        return view('admin.categories.index');
    }
    
    public function store(Request $request){
        $code = 201;
        $message = 'Creado Satisfactoriamente';
        $label = 'success';

        \EduManApp\Categoria::create([
            'desc' => $request->sCreateDesc
        ]);
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }
    
    public function save(Request $request){
        $code = 200;
        $message = "Elemento Actualizado";
        $label = 'success';

        $category = \EduManApp\Categoria::find($request->sEditId);

        if($category == null) {
            $code = 404;
            $message = 'Elemento Inexistente';
            $label = 'danger';
        }else {
            $category->desc = $request->sEditDesc;
            $category->save();
        }

        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }
    
    public function delete(Request $request){
        $code = 200;
        $message = "Elemento Eliminado";
        $label = 'success';

        $elem = \EduManApp\Categoria::find($request->sDestroyId);
        
        if($elem == null) {
            $code = 404;
            $message = 'Elemento Inexistente';
            $label = 'danger';
        }else {
            $elem->delete();
        }

        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }
    
    public function getCreate(Request $request){
        return view('admin.categories.create');
    }
    
    public function getEdit($id, Request $request){
        $category = \EduManApp\Categoria::find($id);
        return view("admin.categories.edit")->with('category', $category);
    }

    public function getDetails($id, Request $request){
        $category = \EduManApp\Categoria::find($id);
        return view('admin.categories.details', ['category'=> $category]);
    }
    
    public function getDelete($id, Request $request){
        $category = \EduManApp\Categoria::find($id);
        return view('admin.categories.delete',['category'=>$category]);
    }
    
    public function getList(Request $request){
        $categories = \EduManApp\Categoria::all();
        return view('admin.categories.list',['categories'=>$categories]);
    }
}
