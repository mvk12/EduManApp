<?php

namespace EduManApp\Http\Controllers;

use Illuminate\Http\Request;

use EduManApp\Http\Requests;

class TutorController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }

    public function index () {
        return view('tutor.index');
    }
}
