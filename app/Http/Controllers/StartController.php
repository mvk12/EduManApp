<?php

namespace EduManApp\Http\Controllers;

use Auth;

use Illuminate\Http\Request;

use EduManApp\Http\Requests;

class StartController extends Controller
{
    public function index () {
        return view('start.index');
    }

    public function About () {
        return view('start.about');
    }

    public function Team () {
        return view('start.team');
    }

    public function Contact () {
        return view('start.contact');
    }

    public function LogIn () {
        $user = Auth::user();
        if ($user == null) {
            return view('start.login');
        } else {
            return redirect('/Admin');
        }
    }

    public function LogOut () {
        Auth::logout();
        return redirect('/LogIn');
    }
    public function SignIn () {
        return view('start.sigin');
    }

    public function Recovery () {
        return view('start.recover');
    }
}

