<?php

namespace EduManApp\Http\Controllers;

use Illuminate\Http\Request;

use EduManApp\Http\Requests;

class TypeActivityController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }
    
    public function index() {
        return view("admin.typeactivities.index");
    }

    public function store(Request $request) {
        $code = 201;
        $message = "Creado Satisfactoriamente!";
        $label = 'success';

        \EduManApp\TypeActivity::create([
            'desc' => $request->sCreateDesc
        ]);
        
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function save(Request $request) {
        $code = 200;
        $message = "Elemento Actualizado";
        $label = 'success';

        $typeactivity = \EduManApp\TypeActivity::find($request->sEditId);

        if ($typeactivity == null) {
            $code = 404;
            $message = 'Elemento Inexistente';
            $label = 'danger';
        } else {
            $typeactivity->desc = $request->sEditDesc;
            $typeactivity->save();
        }
        
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function delete(Request $request){
        $code = 200;
        $message = "Elemento Actualizado";
        $label = 'success';

        $elem = \EduManApp\TypeActivity::find($request->sDestroyId);
        
        if ($elem == null) {
            $code = 404;
            $message = 'Elemento Inexistente';
            $label = 'danger';
        } else {
            $elem->delete();
        }

        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function getEdit($id, Request $request) {
        $typeactivity = \EduManApp\TypeActivity::find($id);

        return view("admin.typeactivities.edit", ['typeactivity' => $typeactivity]);     
    }

    public function getList(Request $request) {
        $tactivities = \EduManApp\TypeActivity::All();
        return view("admin.typeactivities.list", ['tactivities' => $tactivities]);
    }

    public function getCreate(Request $request) {
        return view('admin.typeactivities.create');
    }

    public function getDelete($id, Request $request) {
        $typeactivity = \EduManApp\TypeActivity::find($id);

        return view("admin.typeactivities.delete", ['typeactivity' => $typeactivity]);   
    }
}
