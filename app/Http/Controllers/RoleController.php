<?php

namespace EduManApp\Http\Controllers;

use Illuminate\Http\Request;

use EduManApp\Http\Requests;

class RoleController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }
    
    public function index() {
        return view("admin.roles.index");
    }

    public function store(Request $request) {
        $code = 201;
        $message = "Creado Satisfactoriamente";
        $label = 'success';

        \EduManApp\Role::create([
            'desc' => $request->sCreateDesc
        ]);
        
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function save(Request $request) {
        $code = 200;
        $message = "Elemento Actualizado";
        $label = 'success';

        $role = \EduManApp\Role::find($request->sEditId);

        if ($role == null) {
            $code = 404;
            $message = 'Elemento Inexistente';
            $label = 'danger';
        } else {
            $role->desc = $request->sEditDesc;
            $role->save();
        }
        
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function delete(Request $request){
        $code = 200;
        $message = "Elemento Eliminado";
        $label = 'success';

        $elem = \EduManApp\Role::find($request->sDestroyId);
        
        if ($elem == null) {
            $code = 404;
            $message = "Elemento Inexistente";
            $label = 'danger';
        } else {
            $elem->delete();
        }

        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function getEdit($id, Request $request) {
        $role = \EduManApp\Role::find($id);

        return view("admin.roles.edit")->with('role', $role);     
    }

    public function getList(Request $request) {
        $roles = \EduManApp\Role::All();
        return view("admin.roles.list", compact('roles'));
    }

    public function getCreate(Request $request) {
        return view('admin.roles.create');
    }

    public function getDelete($id, Request $request) {
        $role = \EduManApp\Role::find($id);

        return view("admin.roles.delete")->with('role', $role);   
    }
}
