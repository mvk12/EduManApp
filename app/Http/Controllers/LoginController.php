<?php

namespace EduManApp\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Session;
use Redirect;

use EduManApp\Http\Requests;
use EduManApp\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function LoginPost(Request $req){
        if(Auth::attempt(['username' => $req->sLoginUser, 'password' => $req->sLoginPwd ])) { 
            return Redirect::to('/Admin');
        } else {
            $req->session()->flash('message-error', 'Datos Incorrectos');
            return Redirect::to('LogIn');
        }
    }
}
