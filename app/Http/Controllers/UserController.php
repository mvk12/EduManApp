<?php

namespace EduManApp\Http\Controllers;

use Illuminate\Http\Request;

use EduManApp\Http\Requests;
use EduManApp\Http\Controllers\Controller;

use DB;

class UserController extends Controller
{
    public function __construct () {
        $this->middleware('auth');
    }
    
    public function index() {
        return view('admin.users.index');
    }

    public function store(Request $request) {
        $code = 0;
        $message = ''; 
        $label = 'success';

        DB::beginTransaction();
        try {
            $user = \EduManApp\Usuario::create([
                'username' => $request->sCreateNick,
                'password' => $request->sCreatePwd2,
                'mail' => $request->sCreateMail,
                'role' => $request->sSelecRoleCreateUser
            ]); 

            $perfil = \EduManApp\Perfil::create([
                'id' => $user->id,
                'fullname' => $request->sCreateName,
                'pathimage' => $request->sCreateImagePath
            ]);

            DB::commit();
            $code = 201;
            $message = 'Creado con &eacute;xito';
        } catch(\Exception $ex) {
            DB::rollback();
            $code = 400;
            $message = $ex->getCode() . ' : '. $ex->getMessage();
            $label = 'danger';
        }
        
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function save(Request $request) {
        $user = \EduManApp\Usuario::find($request->sEditId);
        $perfil = \EduManApp\Perfil::find($request->sEditId);
        $code = 0;
        $message = ''; 
        $label = 'success';

        DB::beginTransaction();
        try {
            $user->username = $request->sEditNick;
            $user->mail = $request->sEditMail;
            $user->role = $request->sSelecRoleEditUser;

            $perfil->fullname = $request->sEditName;
            $perfil->pathimage = $request->sEditImagePath;

            $user->save();
            $perfil->save();
            DB::commit();
            $code = 200;
            $message = 'Editado con &eacute;xito';
        } catch(\Exception $ex) {
            DB::rollback();
            $code = 400;
            $message = $ex->getCode() . ' : '. $ex->getMessage();
            $label = 'danger';
        }
        
        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function delete(Request $request) {
        $code = 200;
        $message = 'Eliminado con &eacute;xito';
        $label = 'success';

        #DB::beginTransaction();
        try {
            $user = \EduManApp\Usuario::find($request->sDeleteId);
            if ($user == null) {
                #DB::rollback();
                $code = 404;
                $message = 'Usuario Inexistente';
                $label = 'danger';
            } else {
                $perfil = \EduManApp\Perfil::find($request->sDeleteId);
                $perfil->delete();
                $user->delete();
                
                #DB::commit();
            }
        } catch(\Exception $ex) {
            #DB::rollback();
            $code = 400;
            $message = $ex->getCode() . ' : '. $ex->getMessage();
            $label = 'danger';
        }

        return response()->json(['status' => $code, 'message' => $message, 'label' => $label]);
    }

    public function getCreate(Request $request) {
        $roles = \EduManApp\Role::all();
        return view('admin.users.create')->with('roles', $roles);
    }

    public function getDetails($id, Request $request) {
        $user = DB::table('usuarios')
            ->where('usuarios.id', '=', $id)
            ->join('roles', 'usuarios.role', '=', 'roles.id')
            ->join('perfils', 'usuarios.id', '=', 'perfils.id')
            ->select('usuarios.id','usuarios.username as nickname', 'usuarios.mail', 'usuarios.role as roleid', 'roles.desc as roledesc', 'perfils.fullname', 'perfils.pathimage')
            ->get();
        $json = json_encode((array)$user[0]);
        return view('admin.users.details', ['user' => $user[0], 'json' => $json]);
    }
    
    public function getEdit($id, Request $request) {
        $user = \EduManApp\Usuario::find($id);
        $perfil = \EduManApp\Perfil::find($id);
        $roles = \EduManApp\Role::All();
        return view('admin.users.edit',['user'=>$user, 'perfil' => $perfil ,'roles'=>$roles]);
    }

    public function getDelete($id, Request $request) {
        $user = \EduManApp\Usuario::find($id);
        return view('admin.users.delete')->with('user', $user);
    }

    public function getList(Request $request) {
        $usrlist = DB::table('usuarios')->
            join('roles', 'usuarios.role', '=', 'roles.id')->
            select('usuarios.id as userid', 'usuarios.username as uname', 'usuarios.role as roleid', 'roles.desc as roledesc')->
            get();
        return view('admin.users.list')->with('users', $usrlist);
    }
}
