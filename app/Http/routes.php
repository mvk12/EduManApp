<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Start */
Route::get('/', 'StartController@index');
Route::get('/About', 'StartController@About');
Route::get('/Team', 'StartController@Team');
Route::get('/Contact', 'StartController@Contact');
Route::get('/LogIn', 'StartController@LogIn');
Route::get('/LogOut', 'StartController@LogOut');
Route::get('/SigIn', 'StartController@SignIn');
Route::get('/Recovery', 'StartController@Recovery');

/* Admin */

Route::get('/Admin', 'AdminController@index');
Route::get('/Tutor', 'TutorController@index');
Route::get('/Child', 'ChildController@index');

/* Users */
Route::get('/Admin/Users', 'UserController@index');
Route::get('/Admin/Users/getList', 'UserController@getList');
Route::get('/Admin/Users/getCreate', 'UserController@getCreate');
Route::get('/Admin/Users/getDetails/{id}', 'UserController@getDetails');
Route::get('/Admin/Users/getEdit/{id}', 'UserController@getEdit');
Route::get('/Admin/Users/getDelete/{id}', 'UserController@getDelete');
Route::post('/Admin/Users/store', 'UserController@store');
Route::post('/Admin/Users/save', 'UserController@save');
Route::post('/Admin/Users/delete', 'UserController@delete');
/* Roles */
Route::get('/Admin/Roles', 'RoleController@index');
Route::get('/Admin/Roles/getList', 'RoleController@getList');
Route::get('/Admin/Roles/getCreate', 'RoleController@getCreate');
Route::get('/Admin/Roles/getEdit/{id}', 'RoleController@getEdit');
Route::get('/Admin/Roles/getDelete/{id}', 'RoleController@getDelete');
Route::post('/Admin/Roles/store', 'RoleController@store');
Route::post('/Admin/Roles/save', 'RoleController@save');
Route::post('/Admin/Roles/delete', 'RoleController@delete');
/* Activities */
Route::get('/Admin/Activities', 'ActivityController@index');
Route::get('/Admin/Activities/getList', 'ActivityController@getList');
Route::get('/Admin/Activities/getCreate', 'ActivityController@getCreate');
Route::get('/Admin/Activities/getDetails/{id}', 'ActivityController@getDetails');
Route::get('/Admin/Activities/getEdit/{id}', 'ActivityController@getEdit');
Route::get('/Admin/Activities/getDelete/{id}', 'ActivityController@getDelete');
Route::post('/Admin/Activities/store', 'ActivityController@store');
Route::post('/Admin/Activities/save', 'ActivityController@save');
Route::post('/Admin/Activities/delete', 'ActivityController@delete');
/* TypeActivities */
Route::get('/Admin/TypeActivities', 'TypeActivityController@index');
Route::get('/Admin/TypeActivities/getList', 'TypeActivityController@getList');
Route::get('/Admin/TypeActivities/getCreate', 'TypeActivityController@getCreate');
Route::get('/Admin/TypeActivities/getEdit/{id}', 'TypeActivityController@getEdit');
Route::get('/Admin/TypeActivities/getDelete/{id}', 'TypeActivityController@getDelete');
Route::post('/Admin/TypeActivities/store', 'TypeActivityController@store');
Route::post('/Admin/TypeActivities/save', 'TypeActivityController@save');
Route::post('/Admin/TypeActivities/delete', 'TypeActivityController@delete');
/* Categories */
Route::get('/Admin/Categories', 'CategoryController@index');
Route::get('/Admin/Categories/getList', 'CategoryController@getList');
Route::get('/Admin/Categories/getCreate', 'CategoryController@getCreate');
Route::get('/Admin/Categories/getEdit/{id}', 'CategoryController@getEdit');
Route::get('/Admin/Categories/getDelete/{id}', 'CategoryController@getDelete');
Route::post('/Admin/Categories/store', 'CategoryController@store');
Route::post('/Admin/Categories/save', 'CategoryController@save');
Route::post('/Admin/Categories/delete', 'CategoryController@delete');
/* Login */
Route::post('/Access', 'LoginController@LoginPost');
/* Games */
Route::get('/Game', 'ActChildController@index');