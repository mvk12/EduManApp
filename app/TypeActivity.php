<?php

namespace EduManApp;

use Illuminate\Database\Eloquent\Model;

class TypeActivity extends Model
{
    protected $table = "type_activities";

    protected $fillable = ['desc'];
}
