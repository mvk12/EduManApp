<?php

namespace EduManApp;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = "activities";

    protected $fillable = ['name', 'desc', 'type', 'category'];
}
