<?php

namespace EduManApp;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    protected $table = "usuarios";
    
    protected $fillable = ['username', 'password', 'mail', 'role'];

    protected $hidden = ['password', 'remember_token'];

    public function setPasswordAttribute($value) {
        if(!empty($value)) {
            $this->attributes['password'] = \Hash::make($value);
        }
    }
}
