<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inicio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Handlee|Josefin+Sans|Muli|Noto+Sans" rel="stylesheet">
    {!!Html::style('css/bootstrap.css')!!}
    {!!Html::style('css/home.css')!!}
</head>
<body>
    <div class="nav navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle btn" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="glyphicon glyphicon-user"></span>
                    Men&uacute;
                </button>
                <div class="navbar-brand">
                    <h3>{!!link_to('/', 'EduManApp')!!}</h3>
                </div>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav  navbar-right nav-tabs">
                    <li>{!!link_to('/About', 'Acerca')!!}</li>
                    <li>{!!link_to('/Team', 'Equipo')!!}</li>
                    <li>{!!link_to('/Contact', 'Contacto')!!}</li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            Iniciar Sesión <span class="glyphicon glyphicon-user"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>{!!link_to('/LogIn', 'Entrar')!!}</li>
                            <li>{!!link_to('/SigIn', 'Registrarse')!!}</li>
                        </ul>
                    </li>

                </ul>
            </div>
        </div>
    </div>

    <div class="wrap-content">
        @yield('content')
    </div>

    <footer>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-6">Descripción</div>
                <div class="col-xs-12 col-sm-5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3>¡Escríbenos!</h3>
                            <p>Estamos para ayudarte</p>
                        </div>
                        <div class="panel-body">
                            <form class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon" id="addon-ctcName">Su Nombre </span>
                                </div>
                                <input type="text" class="form-control" id="txtContactName" aria-describedby="addon-ctcName"/>
                                <div class="input-group">
                                    <span class="input-group-addon" id="addon-ctcMail">Su Correo Electrónico </span>
                                </div>
                                <input type="email" class="form-control" id="txtContactName" aria-describedby="addon-ctcMail"/>
                                <div class="input-group">
                                    <span class="input-group-addon" id="addon-ctcMsg">Su comentario </span>
                                </div>
                                <textarea rows="8" cols="30" class="form-control" aria-describedby="addon-ctcMsg"></textarea>
                                <input type="submit" class="btn btn-success btn-block" value="Enviar">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Script Area -->
    {!!Html::script('js/jquery-2.2.4.min.js')!!}
    {!!Html::script('js/bootstrap.min.js')!!}
    @yield('scripts')
</body>
</html>