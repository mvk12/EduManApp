<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inicio</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!!Html::style('css/bootstrap.min.css')!!}
    {!!Html::style('css/animate.min.css')!!}
    {!!Html::style('css/admin.css')!!}
</head>
<body>
    <!-- Navigation -->
    <div class="nav navbar navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle btn" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="glyphicon glyphicon-user"></span>
                    Men&uacute;
                </button>
                <div class="navbar-brand">
                    <a href="javascript:void(0)">Perfil</a>
                </div>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li><a href="javascript:void(0)">Niños</a></li>
                    <li><a href="javascript:void(0)">Calificaciones</a></li>
                    <li><a href="javascript:void(0)">Mensajes</a></li>
                    <li><a href="#">Configuración</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right hidden-xs">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <b>@if(Auth::user() != null) {!!Auth::user()->username!!} @else Unknow @endif</b> <span class="glyphicon glyphicon-user"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Perfil</a></li>
                            <li><a href="/LogOut">Salir</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="nav hidden-sm hidden-md hidden-lg">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <b>User</b> <span class="glyphicon glyphicon-user"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Perfil</a></li>
                            <li><a href="/LogOut">Salir</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Content -->
    @yield('content')

    <div class="progress navbar-fixed-bottom">
        <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="100">
        </div>
    </div>
    <!-- Script Area -->
    {!!Html::script('js/jquery-2.2.4.min.js')!!}
    {!!Html::script('js/bootstrap.min.js')!!}
    {!!Html::script('js/bootstrap-notify.min.js')!!}
    {!!Html::script('js/util/BSAlert.js')!!}
    @yield('scripts')
</body>
</html>
{{--EOF--}}