@extends('templates.start')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="info">
                    <h3>LearnSim</h3>
                    <p> Dedicados al desarrollo </p>    
                </div>
            </div>
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3>¡Escríbenos!</h3>
                        <h4>Estamos para ayudarte</h4>
                    </div>
                    <div class="panel-body">
                        <form class="form">
                            <div class="input-group">
                                <span class="input-group-addon" id="addon-ctcName">Nombre</span>
                                <input type="text" class="form-control" id="txtContactName" aria-describedby="addon-ctcName"/>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon" id="addon-ctcName">Corre</span>
                                <input type="email" class="form-control" id="txtContactName" aria-describedby="addon-ctcName"/>
                            </div>
                            <label>Escriba su comentario</label>
                            <textarea rows="10" cols="1000" class="form-control"></textarea>
                            <br>
                            <input type="submit" class="btn btn-primary btn-block" value="Enviar">
            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection