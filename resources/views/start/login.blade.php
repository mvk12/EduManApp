@extends('templates.start')

@section('content')
<hr />
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h2>Iniciar Sesión</h2>
                </div>
                <div class="panel-body" align="center">
                    <div id="msgSec">
                        @if( Session::has('message-error') )
                        {{Session::get('message-error')}} -> {{Session::get('keys')}}                        
                        @endif
                    </div>
                    <form id="frmLogin" class="form" method="post" action="/Access">
                        <input id="tknLogin" type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <label>Usuario</label>
                        <input id="txtLoginUser" name="sLoginUser" type="text" class="form-control" />
                        <br />
                        <label>Contraseña </label>
                        <input id="txtLoginPwd" name="sLoginPwd" type="password" class="form-control">
                        <br />  
                        <input type="checkbox" />Recordarme
                        <input id="btnLoginSubmit" type="submit" class="btn btn-primary btn-block" value="Enviar">
                    </form>
                </div>
            </div>
        </div>            
    </div>
</div>
@endsection

@section('scripts')
{!!Html::script('js/logic/login.js')!!}
@endsection