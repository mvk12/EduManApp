@extends('templates.start')

@section('content')
<div class="container">
    <div class="row">
		<div class="col-xs-12 col-md-offset-3 col-md-6">
			<div class="panel panel-primary" align="center">
				<div class="panel-heading">
					<h2>Panel de Registro</h2>
				</div>
				<div class="panel-body">
					<form class="form">
						<div class="input-group">
                            <span class="input-group-addon" id="sizing-addon1"><span class="glyphicon glyphicon-user"></span>Nombre</span>
                            <input type="text" class="form-control" placeholder="Nombre" aria-describedby="glyphicon glyphicon-user">
                        </div>
                        <br />
                        <div class="input-group ">
                        <span class="input-group-addon" id="sizing-addon2"><span class="glyphicon glyphicon-user"></span>Usuario</span>
                            <input type="text" class="form-control" placeholder="Usuario" aria-describedby="glyphicon glyphicon-user">
                        </div>  
                        <br />
                        <div class="input-group ">
                            <span class="input-group-addon" id="sizing-addon3"><span class="glyphicon glyphicon-envelope"></span> Correo</span>
                            <input type="email" class="form-control" placeholder="ejempplo:mail@mail" class="form-control" aria-describedby="sizing-addon3">
                        </div>
                        <br />      
                        <div class="input-group">
                            <span class="input-group-addon" id="sizing-addon4"><span class ="glyphicon glyphicon-knight"></span> Contraseña</span>
                            <input type="password" class="form-control" placeholder="contraseña" aria-describedby="glyphicon glyphicon-user">
                        </div>      
                        <br /> 
                        <input type="submit" class="btn btn-primary btn-block" value="Enviar"/>
                    </form>
                </div>
			</div>
		</div>
	</div>            
</div>
@endsection
