@extends('templates.start')

@section('content')
<div id="main_header">
    <div class="container">
        <div class="jumbotron">
            <h1>Titulo</h1>
            <p>Texto descriptivo de la aplicación. No debe ser reduntante, conciso y exacto </p>
            <a href="#" class="btn btn-lg btn-success">Value</a>
        </div>
    </div>
</div>

<div id="descriptions" style="min-height:500px;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <h3>Titulo 1</h3>
                <p>Texto descriptivo</p>
            </div>
            <div class="col-xs-12 col-md-4">
                <h3>Titulo 2</h3>
                <p>Texto Descriptivo</p>
            </div>
            <div class="col-xs-12 col-md-4">
                <h3>Titulo 3</h3>
                <p>Texto Descriptivo</p>
            </div>
        </div>
    </div>
</div>
@endsection