<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Figuras</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{ asset('/assets/game0/estilos.css') }}" rel="stylesheet">
</head>
<body>
  <div id="container" class="container">

    <canvas></canvas>

    <div id="intro" class="cover">
      <p>Pinta sobre las figuras sin salir de ellas.</p>
      <button class="actionbutton">Iniciar</button>
    </div>

    <div id="win" class="cover check">
      <p>¡Bien hecho!</p> 
    </div>

    <div id="error" class="cover">
      <p>Sigue intentando...</p> 
      <button class="actionbutton">Continuar</button>
    </div>
  
    <div id="controles" class="control">
      <button class="undo" id="reload">Siguiente</button>
      <button class="botonsalir" id="salir">X</button>
     <br><br><br><br>
      <label  class="palomita"  id="r" >✔</label>
      <label  class="mala"  id="w" >x</label>
    </div>

  </div>

  <script type="text/javascript" src="{{ asset('/assets/game0/app.js') }}"></script>
</body>
</html>
