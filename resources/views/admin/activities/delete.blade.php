@if(isset($activity))
<form id="frmDeleteactivity" action="/Admin/Activities/delete" method="post">
    <div class="row">
        <div id="delete-alert" class="col-xs-12">
        </div>
        <div class="col-xs-12" align="center">
            <input id="tknDelete" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">¿Desea Eliminar <i id="sDeleteActivity">{{ $activity->name }}</i>?</label>
            <input id="txtDeleteId" type="hidden" name="sDeleteId" value="{{ $activity->id }}" />
        </div>
        <div class="col-xs-12" align="center">
            <button id="btnSubmit_DeleteActivity" type="submit" class="btn btn-success" onclick="handleSubmit_DeleteActivity(event)">Eliminar</button>
            <button id="btnCancel_DeleteActivity" type="reset" class="btn btn-danger" onclick="handleCancel_DeleteActivity(event)">Cancelar</button>
        </div>
    </div>
</form>

@else
@include('util.none')
@endif