@if(!isset($categories) or count($categories) == 0)
<div class="page-header">
    <h3>¡No existen categorías registradas! <br/><small>Crear una <i>Categor&iacute;a</i> primero.</small></h3>
    {!!link_to_action('CategoryController@index', $title = 'Ir a Cat&aacute;logo', $parameters = array(), $attributes = array('class'=>'btn btn-primary'))!!}
</div>
@elseif (!isset($typeactivities) or count($typeactivities) == 0)
<div class="page-header">
    <h3>¡No existen Tipos de Actividades registradas! <br/><small>Crear un <i>Tipo de Actividad</i> primero.</small></h3>
    {!!link_to_action('TypeActivityController@index', $title = 'Ir a Cat&aacute;logo', $parameters = array(), $attributes = array('class'=>'btn btn-primary'))!!}
</div>
@elseif (isset($activity)) 
<div class="panel panel-default">
    <div class="panel-header">
    </div>
    <div class="panel-body">
        <form id="frmActivitiesEdit" action="/Admin/Activities/save" method="post">
            <input id="tknEdit" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input id="tknEditId" type="hidden" name="sEditId" value="{{ $activity->id }}" />
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Nombre</label>
                        <input id="txtEditName" name="sEditName" type="text" class="form-control" value="{{$activity->name}}"/>
                    </div>
                    <div class="col-sm-6">
                        <label for="">Descripci&oacute;n</label>
                        <input id="txtEditDesc" name="sEditDesc" type="text" class="form-control" value="{{$activity->desc}}" />
                    </div>
                    <div class="col-sm-6">
                        <label for="">Tipo Actividad: </label>
                        <select id="selectTypeActivity_EditActivity" name="iTypeActivity_EditActivity" class="form-control" value="{{$activity->type}}">
                        @foreach($typeactivities as $typeactivity)
                            <option value="{{$typeactivity->id}}" @if($typeactivity->id == $activity->type) selected="selected" @endif > {{$typeactivity->desc}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="">Categor&iacute;a: </label>
                        <select id="selectCategory_EditActivity" name="iCategory_EditActivity" class="form-control" value="{{$activity->category}}">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" @if($category->id == $activity->category) selected="selected" @endif > {{$category->desc}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <button id="btnSubmitEdit" type="submit" class="btn btn-success" onclick="handleSubmit_EditActivity(event);">Guardar Cambios</button>
            <button id="btnCancelEdit" type="reset" class="btn btn-danger" onclick="handleCancel_EditActivity(event);">Cancelar Cambios</button>
        </form>
    </div>
</div>
@else
@include('util.none')
@endif
{{--EOF--}}