@if(isset($activities))
<div class="panel panel-primary">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <th>Id</th>
                <th>Nombre</th>
                <th>Categoria</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                @if(count($activities) == 0)
                <tr>
                    <td colspan="4">Sin Datos Para mostrar</td>
                </tr>   
                @else
                @foreach($activities as $activity)
                <tr>
                    <td>{{$activity->id}}</td>
                    <td>{{$activity->name}}</td>
                    <td>{{$activity->category}}</td>
                    <td>
                        <a href="javascript:void(0)" data-id="{{$activity->id}}" onclick="showDetail_Activity($(this));">Detalles</a> | 
                        <a href="javascript:void(0)" data-id="{{$activity->id}}" onclick="showEdit_Activity($(this));">Editar</a> |
                        <a href="javascript:void(0)" data-id="{{$activity->id}}" onclick="showDelete_Activity($(this));">Eliminar</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        {{count($activities)}} elemento(s) encontrado(s).
    </div>
</div>
@else
@include('util.none')
@endif
{{--EOF--}}