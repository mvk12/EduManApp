@if(!isset($categories) or count($categories) == 0)
<div class="page-header">
    <h3>¡No existen categorías registradas! <br/><small>Crear una <i>Categor&iacute;a</i> primero.</small></h3>
    {!!link_to_action('CategoryController@index', $title = 'Ir a Cat&aacute;logo', $parameters = array(), $attributes = array('class'=>'btn btn-primary'))!!}
</div>
@elseif (!isset($typeactivities) or count($typeactivities) == 0)
<div class="page-header">
    <h3>¡No existen Tipos de Actividades registradas! <br/><small>Crear un <i>Tipo de Actividad</i> primero.</small></h3>
    {!!link_to_action('TypeActivityController@index', $title = 'Ir a Cat&aacute;logo', $parameters = array(), $attributes = array('class'=>'btn btn-primary'))!!}
</div>
@else 
<div class="panel panel-default">
    <div class="panel-header">
    </div>
    <div class="panel-body">
        <div id="create-alert"></div>
        <form id="frmActivitiesCreate" action="/Admin/Activities/store" method="post">
            <input id="tknCreate" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="row">
                <div class="col-sm-12">
                    <label for="">Nombre Actividad</label>
                    <input id="txtCreateName" name="sCreateName" type="text" class="form-control"/>
                </div>
                <div class="col-sm-12">
                    <label for="">Descripción</label>
                    <input id="txtCreateDesc" name="sCreateDesc" type="text" class="form-control"/>
                </div>
                <div class="col-sm-6">
                    <label for="">Tipo Actividad: </label>
                    <select id="selectCreate_TypeActivity" name="iTypeActivity_CreateActivity" class="form-control">
                    @foreach($typeactivities as $typeactivity)
                        <option value="{{$typeactivity->id}}">{{$typeactivity->desc}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="col-sm-6">
                    <label for="">Categoría: </label>
                    <select id="selectCreate_Category" name="iCategory_CreateActivity" class="form-control">
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->desc}}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <br/>
            <button id="btnSubmitCreate" type="submit" class="btn btn-success" onclick="handleSubmit_CreateActivity(event);">Crear Nuevo</button>
            <button id="btnCancelCreate" type="reset" class="btn btn-danger" onclick="handleCancel_CreateActivity(event);">Cancelar</button>
        </form>
    </div>
</div>
@endif
{{--EOF--}}