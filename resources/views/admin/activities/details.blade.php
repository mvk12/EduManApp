@if(isset($activity))
<div class="panel panel-default">
    <div class="panel-heading">
        Descripción
    </div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Id: </dt>
            <dd>{{$activity->id}}</dd>
            <dt>Nombre: </dt>
            <dd>{{$activity->name}}</dd>
            <dt>Descripción: </dt>
            <dd>{{$activity->desc}}</dd>
            <dt>Tipo: </dt>
            <dd>{{$activity->type}}</dd>
            <dt>Categoría: </dt>
            <dd>{{$activity->category}}</dd>
        </dl>
        <a href="javascript:void(0)" data-id="{{$activity->id}}" onclick="showEdit_Activity($(this));">Editar</a> 
    </div>
</div>
@else
@include('util.none')
@endif
{{--EOF--}}