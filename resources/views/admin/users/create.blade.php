@if(isset($roles) and count($roles) > 0) 
<div class="panel panel-default">
    <div class="panel-header">
    <h3>Registrar Nuevo</h3>
    </div>
    <div class="panel-body">
        <div id="alert-section"></div>
        <form id="frmUsersCreate" action="/Admin/Users/store" method="post">
            <input id="tknCreate" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <div class="row">
                <div class="col-sm-12">
                    <label for="">Nombre Completo</label>
                    <input id="txtCreateName" name="sCreateName" type="text" class="form-control"/>
                </div>
                <div class="col-sm-6">
                    <label for="">Nombre Usuario</label>
                    <input id="txtCreateNick" name="sCreateNick" type="text" class="form-control"/>
                </div>
                <div class="col-sm-6">
                    <label for="">Rol: </label>
                    <select id="selecRole_CreateUser" name="sSelecRoleCreateUser" class="form-control">
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->desc}}</option>
                    @endforeach
                    </select>
                </div>
                <div class="col-sm-6">
                    <label for="">Contraseña: </label>
                    <input id="txtCreatePwd1" name="sCreatePwd1" type="password" class="form-control"/>
                </div>
                <div class="col-sm-6">
                    <label for="">Repite Contraseña: </label>
                    <input id="txtCreatePwd2" name="sCreatePwd2" type="password" class="form-control"/>
                </div>
                <div class="col-sm-12">
                    <label>Correo: </label>
                    <input id="txtCreateMail" name="sCreateMail"  type="email" class="form-control"/>
                </div>
                <div class="col-sm-12">
                    <label>Nombre Imagen </label>
                    <input id="txtCreateImagePath" name="sCreateImagePath"  type="text" class="form-control"/>
                </div>
            </div>
            <br/>
            <button id="btnSubmitCreate" type="submit" class="btn btn-success" onclick="handleSubmit_CreateUser(event);">Crear</button>
            <button id="btnSubmitCreate" type="reset" class="btn btn-danger" onclick="handleCancel_CreateUser(event);">Cancelar</button>
        </form>
    </div>
</div>
@else
<div class="page-header">
    <h3>¡No existen roles registrados! <br/><small>Crear Roles Primero.</small></h3>
    {!!link_to_action('RoleController@index', $title = 'Ir a Cat&aacute;logo de Roles', $parameters = array(), $attributes = array('class'=>'btn btn-primary'))!!}
</div>
@endif
{{--EOF--}}