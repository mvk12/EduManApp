@if(isset($user))
<div class="panel panel-default">
    <div class="panel-heading">
        Descripción
    </div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Id: </dt>
            <dd>{{$user->id}}</dd>
            <dt>Nombre: </dt>
            <dd>{{$user->fullname}}</dd>
            <dt>Usuario: </dt>
            <dd>{{$user->nickname}}</dd>
            <dt>Rol: </dt>
            <dd>{{$user->roledesc}}</dd>
            <dt>Correo: </dt>
            <dd>{{$user->mail}}</dd>
            <dt>Imagen: </dt>
            <dd>{{$user->pathimage}}</dd>
        </dl>
        <a href="javascript:void(0)" data-id="{{$user->id}}" onclick="showEdit_User($(this));">Editar</a> 
    </div>
</div>
@else
@include('util.none')
@endif
{{--EOF--}}