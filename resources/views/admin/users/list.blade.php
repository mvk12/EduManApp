@if(isset($users))
<div class="panel panel-primary">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <th>Id</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Acciones</th>
            </thead>
            <tbody>
                @if(count($users) == 0)
                <tr>
                    <td colspan="4">Sin Datos Para mostrar</td>
                </tr>   
                @else
                @foreach($users as $user)
                <tr>
                    <td>{{$user->userid}}</td>
                    <td>{{$user->uname}}</td>
                    <td>{{$user->roledesc}}</td>
                    <td>
                        <a href="javascript:void(0)" data-id="{{$user->userid}}" onclick="showDetail_User($(this));">Detalles</a> | 
                        <a href="javascript:void(0)" data-id="{{$user->userid}}" onclick="showEdit_User($(this));">Editar</a> |
                        <a href="javascript:void(0)" data-id="{{$user->userid}}" onclick="showDelete_User($(this));">Eliminar</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
        {{count($users)}} elemento(s) encontrado(s).
    </div>
</div>
@else
@include('util.none')
@endif
{{--EOF--}}