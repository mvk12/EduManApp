@if(isset($user))
<form id="frmDeleteUser" action="/Admin/Users/delete" method="post">
    <div class="row">
        <div id="delete-alert" class="col-xs-12">
        </div>
        <div class="col-xs-12" align="center">
            <input id="tknDelete" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">¿Desea Eliminar <i id="sDeleteUser">{{ $user->username }}</i>?</label>
            <input id="txtDeleteId" type="hidden" name="sDeleteId" value="{{ $user->id }}" />
        </div>
        <div class="col-xs-12" align="center">
            <button id="btnSubmit_DeleteUser" type="submit" class="btn btn-success" onclick="handleSubmit_DeleteUser(event)">Eliminar</button>
            <button id="btnCancel_DeleteUser" type="button" class="btn btn-danger" onclick="handleCancel_DeleteUser(event)">Cancelar</button>
        </div>
    </div>
</form>

@else
@include('util.none')
@endif