@extends('templates.admin')

@section('content')
<input id="tknUsers" type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-sm-7">
            <h3>Usuarios</h3>
        </div>
        <div class="col-xs-6 col-sm-5">
            <button class="btn btn-success" data-toggle="modal" data-target="#mdlCreateUser" role="dialog">
                Nuevo Usuario
            </button>
            <button class="btn btn-primary" onclick="handleList_User(-1);">
                Recargar Lista
            </button>
        </div>
    </div>
    <div class="row">
        <div id="view_list" class="col-xs-12 col-sm-7 col-md-7">
            <!-- List Users Content-->
        </div>
        <div id="view_details" class="col-xs-12 col-sm-5 col-md-5">
            
        </div>
    </div>
</div>

<div id="mdlCreateUser" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="view_create" class="modal-body">
                <!-- Create User Content -->
            </div>
        </div>
    </div>
</div>

<div id="mdlEditUser" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="view_edit" class="modal-body">
                <!-- Edit User Content -->
            </div>
        </div>
    </div>
</div>


<div id="mdlDeleteUser" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="view_delete" class="modal-body">
                <!-- Delete User Content -->
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    {!!Html::script('js/logic/users.js')!!}
@endsection
{{--EOF--}}