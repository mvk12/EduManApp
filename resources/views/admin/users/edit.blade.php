{{-- Roles Exists --}}
@if(!isset($roles) || count($roles) == 0)
<h3>Crear Roles Primero</h3>
{!!link_to_action('RoleController@index', $title = 'Ir a Roles', $parameters = array(), $attributes = array())!!}
@elseif (isset($user) && isset($perfil)) 
<div class="panel panel-default">
    <div class="panel-header">
        <h4>Editar Informaci&oacute;n</h4>
    </div>
    <div class="panel-body">
        <div id="alert-section-edit"></div>
        <form id="frmUsersEdit" action="/Admin/Users/save" method="post">
            <input id="tknEdit" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input id="txtEditId" type="hidden" name="sEditId" value="{{$user->id}}" />
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="">Nombre Completo</label>
                        <input id="txtEditName" name="sEditName" type="text" class="form-control" value="{{$perfil->fullname}}"/>
                    </div>
                    <div class="col-sm-6">
                        <label for="">Nombre Usuario</label>
                        <input id="txtEditNick" name="sEditNick" type="text" class="form-control" value="{{$user->username}}" />
                    </div>
                    <div class="col-sm-6">
                        <label for="">Rol: </label>
                        <select id="selecRole_EditUser" name="sSelecRoleEditUser" class="form-control" value="{{$user->role}}">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}" @if($user->role == $role->id) selected="selected" @endif > {{$role->desc}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="col-sm-12">
                        <label>Correo: </label>
                        <input id="txtEditMail" name="sEditMail" type="email" class="form-control" value="{{$user->mail}}" />
                    </div>
                    <div class="col-sm-12">
                        <label>Path Image: </label>
                        <input id="txtEditImagePath" name="sEditImagePath" type="text" class="form-control" value="{{$perfil->pathimage}}" />
                    </div>
                </div>
            </div>
            <button id="btnSubmitEdit" type="submit" class="btn btn-success" onclick="handleSubmit_EditUser(event);">Guardar Cambios</button>
            <button id="btnCancelEdit" type="reset" class="btn btn-danger" onclick="handleCancel_EditUser(event);">Cancelar Cambios</button>
        </form>
        <hr/>
        <button class="btn btn-primary" onclick="handleAllowChangePassword(event);">Pulse aquí para cambiar contraseña</button>
        <form id="frmUserChangePwd" action="/Admin/Users/savePwd" method="post">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <label for="">Contraseña: </label>
                                    <input id="txtEditPwd1" name="sEditPwd1" type="password" class="form-control" disabled="disabled"/>
                                </div>
                                <div class="col-sm-6">
                                    <label for="">Repite Contraseña: </label>
                                    <input id="txtEditPwd2" name="sEditPwd2" type="password" class="form-control" disabled="disabled"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <button id="btnSubmitChngPwd" type="submit" class="btn btn-success" onclick="handleSubmit_ChngPwd(event);">Cambiar Contrase&ntilde;a</button>
                        <button id="btnCancelChngPwd" type="reset" class="btn btn-danger" onclick="handleCancel_ChngPwd(event);">Cancelar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@else
@include('util.none')
@endif
{{--EOF--}}