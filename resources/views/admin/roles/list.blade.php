@if(isset($roles))
<div class="panel panel-primary">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Rol</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @if(count($roles) == 0)
                <tr>
                    <td colspan="3">Sin Datos Para mostrar</td>
                </tr>    
                @else
                @foreach($roles as $role)
                <tr class="role-row">
                    <td>{{$role->id}}</td>
                    <td>{{$role->desc}}</td>
                    <td>
                        <a href="javascript:void(0)" data-id="{{$role->id}}" onclick="showDetail_Role($(this));">Detalles</a> | 
                        <a href="javascript:void(0)" data-id="{{$role->id}}" onclick="showEdit_Role($(this));">Editar</a> |
                        <a href="javascript:void(0)" data-id="{{$role->id}}" onclick="showDelete_Role($(this));">Eliminar</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
    {{count($roles)}} elemento(s) encontrado(s).
    </div>
</div>
@else
@include('util.none')
@endif