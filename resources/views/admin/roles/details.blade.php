@if(isset($role))
<div class="panel panel-default">
    <div class="panel-heading">
        Descripción
    </div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Id</dt>
            <dd>{{$role->id}}</dd>
            <dt>Descripción</dt>
            <dd>{{$role->desc}}</dd>
        </dl>
        <a href="javascript:void(0)" data-id="{{$role->id}}" onclick="showEdit_Role($(this));">Editar</a> 
    </div>
</div>
@else
@include('util.none')
@endif