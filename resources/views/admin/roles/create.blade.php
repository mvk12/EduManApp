<form id="frmCreateRole" action="/Admin/Roles/store" method="post">
    <div class="row">
        <div id="create-alert" class="col-xs-12">
        </div>
        <div class="col-xs-3">
            <input id="tknCreate" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">Rol: </label>
        </div>
        <div class="col-xs-6">
            <input id="txtCreateDesc" name="sCreateRole" type="text" class="form-control" />
        </div>
        <div class="col-xs-3">
            <input id="btnSubmitCreate" type="submit" value="Guardar" class="btn btn-success" onclick="handleSubmit_CreateRole(event);" />
        </div>
    </div>
</form>