@extends('templates.admin')

@section('content')
<input id="tknfrmList_Role" type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-sm-7">
            <h3>Roles</h3>
        </div>
        <div class="col-xs-6 col-sm-5">
            <button class="btn btn-success" data-toggle="modal" data-target="#mdlCreateRole" role="dialog">
                Nuevo Rol
            </button>
            <button class="btn btn-primary" onclick="handleList_Role(-1);">
                Recargar Lista
            </button>
        </div>
    </div>
    <div class="row">
        <div id="view_list" class="col-xs-12 col-sm-7 col-md-7">
            
        </div>
        <div id="viewDetails" class="col-xs-12 col-sm-5 col-md-5">

        </div>
    </div>
</div>

<div id="mdlCreateRole" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Registrar Nuevo</h3>
            </div>
            <div id="view_create" class="modal-body">
                <!-- Create Area -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">
                    Regresar
                </button>
            </div>
        </div>
    </div>
</div>

<div id="mdlEditRole" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4>Editar</h4>
            </div>
            <div id="view_edit" class="modal-body">
                <!-- Edit Area -->
            </div>
        </div>
    </div>
</div>

<div id="mdlDeleteRole" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="view_delete" class="modal-body">
                <!-- Delete Role -->
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    {!!Html::script('js/logic/roles.js')!!}
@endsection
{{--EOF--}}