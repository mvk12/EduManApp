@if($role != null)
<form id="frmDeleteRole" action="/Admin/Roles/delete" method="post">
    <div class="row">
        <div id="delete-alert" class="col-xs-12">
        </div>
        <div class="col-xs-12" align="center">
            <input id="tknDelete" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">¿Desea Eliminar <i id="sDeleteRole">{{ $role->desc }}</i>?</label>
            <input id="txtDestroyId" type="hidden" name="sDestroyId" value="{{ $role->id }}" />
        </div>
        <div class="col-xs-12" align="center">
            <button id="btnSubmit_DeleteRole" type="submit" class="btn btn-success" onclick="handleSubmit_DeleteRole(event)">Eliminar</button>
            <button id="btnCancel_DeleteRole" type="button" class="btn btn-danger" onclick="handleCancel_DeleteRole(event)">Cancelar</button>
        </div>
    </div>
</form>
@else
@include('util.none')
@endif