@if($role != null)
<form id="frmEditRole" action="/Admin/Roles/save" method="post">
    <div class="row">
        <div id="edit-alert" class="col-xs-12">
        </div>
        <div class="col-xs-12 col-sm-3">
            <input id="tknEdit" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input id="txtEditId" type="hidden" name="sEditId" value="{{ $role->id }}" />
            <label for="">Editar Rol: </label>
        </div>
        <div class="col-xs-12 col-sm-6">
            <input id="txtEditDesc" name="sEditRole" type="text" class="form-control" value="{{$role->desc}}" />
        </div>
        <br>
        <div class="col-xs-12 col-sm-9 col-sm-offset-3">
            <input id="btnSubmitEdit" type="submit" value="Guardar Cambios" class="btn btn-success" onclick="handleSubmit_EditRole(event);" />
            <input id="btnCancelEdit" type="button" value="Descartar Cambios" class="btn btn-danger" onclick="handleIgnore_EditRole(event);" />
        </div>
    </div>
</form>
@else
<div class="page-header">
    <h3>Error de Búsqueda. <br/><small>El elemento que buscas no existe o se ejecutó un parametro inadecuado</small></h3>
</div>
@endif