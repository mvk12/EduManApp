@extends('templates.admin')

@section('content')
<input id="tknCategories" type="hidden" name="_token" value="{{ csrf_token() }}" />
<div class="container">
    <div class="row">
        <div class="col-xs-6 col-sm-7">
            <h3>Categorías</h3>
        </div>
        <div class="col-xs-6 col-sm-5">
            <button class="btn btn-success" data-toggle="modal" data-target="#mdlCreateCategory" role="dialog">
                Nueva Categoría
            </button>
            <button class="btn btn-primary" onclick="handleList_Category(-1);">
                Recargar Lista
            </button>
        </div>
    </div>
    <div class="row">
        <div id="view_list" class="col-xs-12 col-sm-7 col-md-7">
            <!-- List Categories -->      
        </div>
        <div id="view_details" class="col-xs-12 col-sm-5 col-md-5">
            <!-- Details Category -->
        </div>
    </div>
</div>
{{-- Modal Category Region --}}
<div id="mdlCreateCategory" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3>Registrar Nuevo</h3>
            </div>
            <div id="view_create" class="modal-body">
                <!-- Create Category -->
            </div>
        </div>
    </div>
</div>

<div id="mdlEditCategory" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="view_edit" class="modal-body">
                <!-- Edit Category -->
            </div>
        </div>
    </div>
</div>

<div id="mdlDeleteCategory" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="view_delete" class="modal-body">
                <!-- Delete Category -->
            </div>
        </div>
    </div>
</div>
{{--  --}}
@endsection

@section('scripts')
    {!!Html::script('js/logic/categories.js')!!}
@endsection
{{--EOF--}}