@if(isset($categories))
<div class="panel panel-primary">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Descripcion</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @if(count($categories) == 0)
                <tr>
                    <td colspan="3">Sin Datos Para mostrar</td>
                </tr>    
                @else
                @foreach($categories as $categoria)
                <tr class="role-row">
                    <td>{{$categoria->id}}</td>
                    <td>{{$categoria->desc}}</td>
                    <td>
                        <a href="#" data-id="{{$categoria->id}}" onclick="showDetail_Category($(this));">Detalles</a> | 
                        <a href="#" data-id="{{$categoria->id}}" onclick="showEdit_Category($(this));">Editar</a> |
                        <a href="#" data-id="{{$categoria->id}}" onclick="showDelete_Category($(this));">Eliminar</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
    {{count($categories)}} elemento(s) encontrado(s).
    </div>
</div>   
@else
@include('util.none')
@endif