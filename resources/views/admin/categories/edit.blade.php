@if($category != null)
<form id="frmEditCategory" action="/Admin/Categories/save" method="post">
    <div class="row">
        <div id="edit-alert" class="col-xs-12">
        </div>
        <div class="col-xs-12 col-sm-3">
            <input id="tknEdit" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <input id="txtEditId" type="hidden" name="sEditId" value="{{ $category->id }}" />
            <label for="">Editar Categor&iacute;a: </label>
        </div>
        <div class="col-xs-12 col-sm-6">
            <input id="txtEditDesc" name="sEditCategory" type="text" class="form-control" value="{{$category->desc}}" />
        </div>
        <br>
        <div class="col-xs-12 col-sm-9 col-sm-offset-3">
            <button id="btnSubmitEdit" type="submit" class="btn btn-success" onclick="handleSubmit_EditCategory(event);">Guardar Cambios</button>
            <button id="btnCancelEdit" type="reset"  class="btn btn-danger" onclick="handleCancel_EditCategory(event);">Descartar Cambios</button>
        </div>
    </div>
</form>
@else
@include('util.none')
@endif