@if(isset($category))
<div class="panel panel-default">
    <div class="panel-heading">
        Descripción
    </div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Id</dt>
            <dd>{{$category->id}}</dd>
            <dt>Descripci&oacute;n</dt>
            <dd>{{$category->desc}}</dd>
        </dl>
        <a href="javascript:void(0)" data-id="{{$category->id}}" onclick="showEdit_Category($(this));">Editar</a> 
    </div>
</div>
@else
@include('util.none')
@endif