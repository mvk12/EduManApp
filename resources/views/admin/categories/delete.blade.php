@if($category != null)
<form id="frmDeleteCategory" action="/Admin/Categories/delete" method="post">
    <div class="row">
        <div id="delete-alert" class="col-xs-12">
        </div>
        <div class="col-xs-12" align="center">
            <input id="tknDelete" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">¿Desea Eliminar <i id="sDeleteCategory">{{ $category->desc }}</i>?</label>
            <input id="txtDestroyId" type="hidden" name="sDestroyId" value="{{ $category->id }}" />
        </div>
        <div class="col-xs-12" align="center">
            <button id="btnSubmit_DeleteCategory" type="submit" class="btn btn-success" onclick="handleSubmit_DeleteCategory(event)">Eliminar</button>
            <button id="btnCancel_DeleteCategory" type="reset" class="btn btn-danger" onclick="handleCancel_DeleteCategory(event)">Cancelar</button>
        </div>
    </div>
</form>
@else
@include('util.none')
@endif