<form id="frmCreateCategory" action="/Admin/Categories/store" method="post">
    <div class="row">
        <div id="create-alert" class="col-xs-12">
        </div>
        <div class="col-xs-3">
            <input id="tknCreate" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">Categor&iacute;a: </label>
        </div>
        <div class="col-xs-6">
            <input id="txtCreateDesc" name="sCreateCategory" type="text" class="form-control" />
        </div>
        <div class="col-xs-3">
            <button id="btnSubmitCreate" type="submit" class="btn btn-success" onclick="handleSubmit_CreateCategory(event);">Crear Nuevo</button>
            <button id="btnCancelCreate" type="reset"  class="btn btn-danger" onclick="handleCancel_CreateCategory(event);">Cancelar</button>
        </div>
    </div>
</form>