@if(isset($tactivities))
<div class="panel panel-primary">
    <div class="panel-heading">
    </div>
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Rol</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                @if(count($tactivities) == 0)
                <tr>
                    <td colspan="3">Sin Datos Para mostrar</td>
                </tr>    
                @else
                @foreach($tactivities as $typeactivity)
                <tr class="typeactivity-row">
                    <td>{{$typeactivity->id}}</td>
                    <td>{{$typeactivity->desc}}</td>
                    <td>
                        <a href="javascript:void(0)" data-id="{{$typeactivity->id}}" onclick="showDetail_TypeActivity($(this));">Detalles</a> | 
                        <a href="javascript:void(0)" data-id="{{$typeactivity->id}}" onclick="showEdit_TypeActivity($(this));">Editar</a> |
                        <a href="javascript:void(0)" data-id="{{$typeactivity->id}}" onclick="showDelete_TypeActivity($(this));">Eliminar</a>
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
    <div class="panel-footer">
    {{count($tactivities)}} elemento(s) encontrado(s).
    </div>
</div>
@else
@include('util.none')
@endif