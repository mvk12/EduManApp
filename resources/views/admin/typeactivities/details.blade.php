@if(isset($typeactivity))
<div class="panel panel-default">
    <div class="panel-heading">
        Descripción
    </div>
    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Id</dt>
            <dd>{{$typeactivity->id}}</dd>
            <dt>Descripción</dt>
            <dd>{{$typeactivity->desc}}</dd>
        </dl>
        <a href="javascript:void(0)" data-id="{{$typeactivity->id}}" onclick="showEdit_TypeActivity($(this));">Editar</a> 
    </div>
</div>
@else
@include('util.none')
@endif