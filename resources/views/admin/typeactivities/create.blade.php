<form id="frmCreateTypeActivity" action="/Admin/TypeActivities/store" method="post">
    <div class="row">
        <div id="create-alert" class="col-xs-12">
        </div>
        <div class="col-xs-3">
            <input id="tknCreate" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">Tipo de Actividad: </label>
        </div>
        <div class="col-xs-6">
            <input id="txtCreateDesc" name="sCreateTypeActivity" type="text" class="form-control" />
        </div>
        <div class="col-xs-3">
            <input id="btnSubmitCreate" type="submit" value="Guardar" class="btn btn-success" onclick="handleSubmit_CreateTypeActivity(event);" />
        </div>
    </div>
</form>