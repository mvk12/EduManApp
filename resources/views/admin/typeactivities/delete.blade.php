@if($typeactivity != null)
<form id="frmDeleteTypeActivity" action="/Admin/TypeActivities/delete" method="post">
    <div class="row">
        <div id="delete-alert" class="col-xs-12">
        </div>
        <div class="col-xs-12" align="center">
            <input id="tknDelete" type="hidden" name="_token" value="{{ csrf_token() }}" />
            <label for="">¿Desea Eliminar <i id="sDeleteTypeActivity">{{ $typeactivity->desc }}</i>?</label>
            <input id="txtDestroyId" type="hidden" name="sDestroyId" value="{{ $typeactivity->id }}" />
        </div>
        <div class="col-xs-12" align="center">
            <button id="btnSubmit_DeleteTypeActivity" type="submit" class="btn btn-success" onclick="handleSubmit_DeleteTypeActivity(event)">Eliminar</button>
            <button id="btnCancel_DeleteTypeActivity" type="button" class="btn btn-danger" onclick="handleCancel_DeleteTypeActivity(event)">Cancelar</button>
        </div>
    </div>
</form>
@else
@include('util.none')
@endif